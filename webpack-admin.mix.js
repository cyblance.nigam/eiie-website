const mix = require("laravel-mix");
/* 

  MIX config for compiling admin app, run with environment variable
MIX_FILE=webpack-admin.mix npm run dev

*/
require("laravel-mix-eslint");

mix.sass("resources/sass/style.scss", "public/css")
    .ts("resources/js/app.tsx", "public/js")
    .sourceMaps()
    .eslint({
        fix: true,
        extensions: ["ts", "tsx"],
    })
    .js("resources/js/script.js", "public/js");
// .browserSync("localhost:8000");
