<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// files
Route::get('/img/original_{path}', fn($path) => redirect()->route('img', ['path' => $path], 301));

Route::get('/img/{path}', 'ImageController@show')
	->where('path', '.*')
	// ->middleware('cache.headers:public;max_age=2628000;etag')
	->name('img');
Route::get('/media_gallery/{path}', fn($path) => 
	redirect()->route('img', ['path' => str_replace('original_','',$path)], 301));
Route::get('/file/{file}/{name?}', 'FileController@download')->name('file');

Route::prefix(request()->segment(1))
		->middleware(['localized'])
		->group(function() {

	/* Generic routes */
	Route::get('/item/{id}:{slug?}', 'ItemController@show')
			->name('item.show');

	Route::get('/collection/{id}:{slug?}', 'CollectionController@show')
			->name('collection');

	Route::get('/country/{id}:{slug?}', 'CollectionController@showCountry')
			->name('country');
	Route::get('/country/{id}:{slug?}/details/{slotId}:{slotSlug?}', 
			'CollectionController@showCountrySlot')
			->name('country.slot');
	Route::get('/country/{id}:{slug?}/articles', 'CollectionController@listCountryArticles')
			->name('country.articles');
	Route::get('/country/{id}:{slug?}/development-cooperation-projects', 'CollectionController@listCountryDCProjects')
			->name('country.dcprojects');

			

	Route::get('/region/{id}:{slug?}', 'CollectionController@show')
			->name('region');

	Route::get('/author/{id}:{slug?}', 'CollectionController@show')
			->name('author');
	Route::get('/authors', 'CollectionController@listAuthors')
			->name('authors');

	Route::get('/dossier/{id}:{slug?}', 'CollectionController@show')
			->name('dossier');
	Route::get('/dossier/{parent_id}:{parent_slug?}/{id}:{slug?}', 'CollectionController@showSub')
			->name('dossier.sub');
	Route::get('/spotlight', 'CollectionController@show')
			->defaults('id', config('eiie.collection.spotlight'))
			->name('spotlight');
	Route::get('/campaigns', 'CollectionController@show')
			->defaults('id', config('eiie.collection.campaigns'))
			->name('campaigns');
	
	Route::get('/workarea/{id}:{slug?}', 'CollectionController@show')
			->name('workarea');
	
	/* STATIC routes */
	Route::get('/', 'HomeController@show')
			->name('home');
	Route::get('/news', 'CollectionController@show')
		->defaults('id', config('eiie.collection.news'))
		->name('news');
	Route::get('/regions', 'CollectionController@show')
		->defaults('id', config('eiie.collection.regions-group'))
		->name('regions');
	Route::get('/priorities', 'CollectionController@show')
		->defaults('id', config('eiie.collection.priorities'))
		->name('priorities');
	Route::get('/affiliates', 'ItemController@listAffiliates')
		->name('affiliates');
	Route::get('/api/affiliates', 'ApiController@listAffiliates')->name('api.affiliates');

	Route::get('/search', 'SearchController@show')
		->name('search');
	// Route::get('/search/{keyword}', 'SearchController@search')
	// 	->name('search-keyword');

	Route::get('/news/take-action', 'CollectionController@show')
			->defaults('id', config('eiie.collection.take-action'))
			->name('take-action');
	Route::get('/opinion', 'CollectionController@show')
			->defaults('id', config('eiie.collection.opinion'))
			->name('opinion');
	Route::get('/news/statements', 'CollectionController@show')
			->defaults('id', config('eiie.collection.statements'))
			->name('statements');

			

	/* ABOUT */
	Route::prefix('about')->name('about.')->group(function() {
		Route::get('/who-we-are', 'ItemController@show')
			->defaults('id', config('eiie.item.who-we-are'))
			->name('who-we-are');
		Route::get('/origins-and-history', 'ItemController@showByOldId')
			->defaults('id', config('eiie.old_item.origins-and-history'))
			->name('origins-and-history');
		Route::get('/principal-aims', 'ItemController@showByOldId')
			->defaults('id', config('eiie.old_item.principal-aims'))
			->name('principal-aims');
		Route::get('/global-unions', 'ItemController@showByOldId')
			->defaults('id', config('eiie.old_item.global-unions'))
			->name('global-unions');
		Route::get('/jobs', 'CollectionController@show')
			->defaults('id', config('eiie.collection.jobs'))
			->name('jobs');
		Route::get('/call-for-proposals', 'ItemController@showByOldId')
			->defaults('id', 15649)
			->name('call-for-proposals');	
		Route::get('/our-leaders', 'CollectionController@show')
			->defaults('id', config('eiie.collection.executive-board'))
			->name('leaders');
		Route::get('/our-team', 'CollectionController@show')
			->defaults('id', config('eiie.collection.staff'))
			->name('team');
	});
	/* GOVERNANCE */
	Route::get('/governance', 'CollectionController@show')
		->defaults('id', config('eiie.collection.governance'))
		->name('governance');
	Route::prefix('governance')->name('governance.')->group(function() {
		Route::get('/constitution-and-bylaws', 'CollectionController@show')
			->defaults('id', config('eiie.collection.constitutional-documents'))
			->name('constitution-and-bylaws');
		Route::get('/world-congress', 'CollectionController@show')
			->defaults('id', config('eiie.collection.world-congress'))
			->name('world-congress');
		Route::get('/annual-reports', 'CollectionController@show')
			->defaults('id', config('eiie.collection.annual-reports'))
			->name('annual-reports');
	});
	
	/* CONTACT */
	Route::prefix('contact')->name('contact.')->group(function() {
		Route::get('/ei-ie', 'ContactController@show')
			->name('ei-ie');
		Route::post('/ei-ie', 'ContactController@contactPost')
			->middleware('recaptcha.score')
			->name('form-post');
		// Route::get('/members', 'ItemController@listAffiliates')
		// 	->name('members');
		Route::get('/legal-notice', 'ItemController@showByOldId')
			->defaults('id', 4380)
			->name('legal-notice');
		Route::get('/data-protection-policy', 'ItemController@showByOldId')
			->defaults('id', 15833)
			->name('data-protection-policy');
		Route::get('/newsletter', 'NewsletterController@show')
			->name('newsletter')
			;
	});

	Route::prefix('resources')->name('resources.')->group(function() {
		Route::get('/world-congress-resolutions', 'CollectionController@show')
			->defaults('id', config('eiie.collection.congress-resolutions'))
			->name('world-congress-resolutions');
		Route::get('/publications-and-research', 'CollectionController@show')
			->defaults('id', config('eiie.collection.publications-and-research'))
			->name('publications-and-research');
		Route::get('/policy-briefs', 'CollectionController@show')
			->defaults('id', config('eiie.collection.policy-briefs'))
			->name('policy-briefs');
		Route::get('/videos', 'ItemController@listResources')
			->defaults('subtype', 'video')
			->name('videos');	
		Route::get('/podcasts', 'SoundCloudController@show')
			->name('podcasts');	
		Route::get('/development-cooperation-projects', 'ItemController@listDCProjects')
			->name('dcprojects');			
	});


	// Old site redirects
	Route::get('/child_dossier', fn() => redirect()->route('spotlight', [], 301));
	Route::get('/contactus', fn() => redirect()->route('contact.ei-ie', [], 301));
	Route::get('/country_profile_list', fn() => redirect()->route('regions', [], 301));
	Route::get('/country-profile-page/{id?}/{section_id?}/{title?}', 'RedirectController@country_profile');
	Route::get('/country-profile', fn() => redirect()->route('regions', [], 301));
	Route::get('/dco_project/{country_id?}/{post_id?}/{title?}', fn() => redirect()->route('dcprojects'));
	Route::get('/dco', fn() => redirect()->route('dcprojects'));
	Route::get('/dco/{country_id?}', fn() => redirect()->route('dcprojects'));
	Route::get('/detail_eb/{id}/{slug?}', 'RedirectController@detail_executive_board');
	Route::get('/detail_page/{id}/{slug?}', 'RedirectController@detail_page');	
	Route::get('/detail_staff/{id?}/{title?}', fn() => redirect()->route('about.team', [], 301));
	Route::get('/detail/{id}/{slug?}', 'RedirectController@detail');
	Route::get('/documents', fn() => redirect()->route('resources.publications-and-research', [], 301));
	Route::get('/dossier', fn() => redirect()->route('spotlight', [], 301));
	Route::get('/dossierdetail/{id}/{slug?}', 'RedirectController@dossierdetail');
	Route::get('/eiie-organizations', fn() => redirect()->route('affiliates'));
	Route::get('/executive_board', fn() => redirect()->route('about.leaders', [], 301));
	Route::get('/issues_actions', fn() => redirect()->route('take-action'));
	Route::get('/media_detail/{id?}/{slug?}', 'RedirectController@detail_media');
	Route::get('/media-list', fn() => redirect()->route('resources.videos', [], 301));
	Route::get('/medias', fn() => redirect()->route('resources.videos', [], 301));
	Route::get('/members', fn() => redirect()->route('affiliates'));
	Route::get('/podcasts', fn() => redirect()->route('resources.podcasts', [], 301));
	Route::get('/reseach_feed/{pageNo?}/{position?}', fn() => redirect()->route('resources.publications-and-research', [], 301));
	Route::get('/resolutions/constitution', fn() =>redirect()->route('governance.constitution-and-bylaws', 301));
	Route::get('/resolutions/resolutions', fn() => redirect()->route('resources.world-congress-resolutions', 301));
	Route::get('/resolutions/statements', fn() =>redirect()->route('collection', ['id' => config('eiie.collection.general-ei-declaration'), 'slug' => '-'], 301));
	Route::get('/searchitem', fn() => redirect()->route('search', [], 301));
	Route::get('/sound_cloud', fn() => redirect()->route('resources.podcasts', [], 301));
	Route::get('/soundcloud-tracks', fn() => redirect()->route('resources.podcasts', [], 301));
	Route::get('/staff_profiles', fn() => redirect()->route('about.team', [], 301));
	Route::get('/woe_homepage', fn() => redirect()->route('home', [], 301));
	Route::get('/woe_homepage/blog_article_list', fn() => redirect()->route('opinion', [], 301));
	Route::get('/woe_homepage/contact_us', fn() => redirect()->route('contact.ei-ie', [], 301));
	Route::get('/woe_homepage/contactus', fn() => redirect()->route('contact.ei-ie', [], 301));
	Route::get('/woe_homepage/detail_page/{id}/{slug?}', 'RedirectController@detail_page');
	Route::get('/woe_homepage/woe_detail_page/{id}/{slug?}', 'RedirectController@detail_page');
	Route::get('/woe_homepage/publications/{type?}', fn() => redirect()->route('resources.publications-and-research', [], 301));
	Route::get('/woe_homepage/resource_links', fn() => redirect()->route('resources.publications-and-research', [], 301));
	Route::get('/woe_homepage/resource_page', fn() => redirect()->route('resources.publications-and-research', [], 301));
	Route::get('/woe_homepage/video_detail/{id?}/{title?}', fn() => redirect()->route('resources.videos', [], 301));
	Route::get('/woe_homepage/videos', fn() => redirect()->route('resources.videos', [], 301));
	Route::get('/woe_homepage/detail/{id}/{slug?}', 'RedirectController@detail');
	Route::get('/woe_homepage/woe_detail/{id}/{slug?}', 'RedirectController@detail');


	// End locale grouping
});

//azure login route & callback added by Cyblance
Route::get('/login/azure', '\App\Http\Middleware\AppAzure@azure')->name('azure');
Route::get('/login/azurecallback', '\App\Http\Middleware\AppAzure@azurecallback');

Route::get('login', 'Admin\LoginController@login')
	->name('login');
Route::post('login', 'Admin\LoginController@authenticate')
	->name('login.submit');
Route::post('logout', 'Admin\LoginController@logout')
	->name('logout');
Route::get('login/forgot-password', 'Admin\LoginController@passwordForgot')
	->name('password.request');
Route::post('login/forgot-password', 'Admin\LoginController@passwordRequest')
	->name('password.email');
Route::get('login/reset-password/{token}', 'Admin\LoginController@passwordReset')
	->name('password.reset');
Route::post('login/reset-password', 'Admin\LoginController@passwordUpdate')
	->name('password.update');

Route::prefix('admin')
	->name('admin.')
	->middleware(['auth'])
	->group(function() {
		Route::get('/', 'Admin\DashboardController@home')
			->name('dashboard');
		Route::resource('collections', Admin\CollectionController::class);
		Route::resource('items', Admin\ItemController::class);
		Route::resource('resourceitems', Admin\ResourceItemController::class);
		Route::resource('users', Admin\UserController::class);
		
		Route::post('users/{user}/resetPassword', 'Admin\UserController@resetPassword')
			->name('users.resetPassword');

		Route::post('collections/{id}/items/ordering', 'Admin\CollectionController@itemsOrdering')
			->name('collection.items.ordering');
		Route::post('collections/{id}/items/import', 'Admin\CollectionController@itemsImport')
			->name('collection.items.import');
		Route::get('collections/{ids}/items/count', 'Admin\CollectionController@itemsCount')
			->name('collections.items.count');
		
		Route::prefix('resourceitem')
			->name('resourceitem.')
			->group(function() {
				Route::post('file/upload', 'Admin\ResourceItemController@uploadFile')
					->name('file.upload');
				Route::post('image/replace', 'Admin\ResourceItemController@replaceImage')
					->name('image.replace');
			});		
});

Route::get('/clear-cache', function() {

    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    // return what you want
    echo 'Cache succussfully cleared.';
    exit();
});
