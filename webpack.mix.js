const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

/* @Thorben: you can comment out the .ts rule for faster compiling */
/* Comment out by appending // in front */

// mix.webpackConfig({
//     resolve: {
//         symlinks: false,
//     },
// });
mix.sass("resources/sass/style.scss", "public/css")
    .ts("resources/js/app.tsx", "public/js")
    .js("resources/js/script.js", "public/js");

if (true || mix.inProduction()) {
    mix.version();
}
