<?php

namespace App\Models;


class ResourceImage extends Model
{
    public $timestamps = false;
  
    public function content() {
        return $this->belongsTo(ItemContent::class);
    }

}
