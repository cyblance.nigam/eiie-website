<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Stevebauman\Purify\Facades\Purify;
use App\Scopes\LanguageScope;
use Illuminate\Support\Str;
use OwenIt\Auditing\Contracts\Auditable;

class ItemContent extends Model implements Auditable
{
    use Searchable;
    use \OwenIt\Auditing\Auditable;

    protected $casts = [
        // 'resources' => 'array',
        'footnotes' => 'array',
        'meta' => 'array'
    ];
    // re-enable touches after seeding!
    // protected $touches = ['item'];
    // protected $withCount = ['images', 'videos'];

    protected static function booted() {
        static::addGlobalScope(new LanguageScope);
    }

    public function setTitleAttribute($val) {
        $this->attributes['title'] = $val;
        // always override slug
        $this->attributes['slug'] = Str::slug($val);
    }
    
    public function item() {
        return $this->belongsTo('App\Models\Item');
    }

    public function images() {
        return $this->hasMany(ResourceImage::class, 'content_id');
    }
    public function image() {
        return $this->images()->first();
    }

    public function videos() {
        return $this->hasMany(ResourceVideo::class, 'content_id');
    }
    public function video() {
        return $this->videos()->first();
    }

    public function links() {
        return $this->hasMany(ResourceLink::class, 'content_id');
    }

    public function files() {
        return $this->hasMany(ResourceFile::class, 'content_id')->orderBy('resource_files.order');
    }

    public function contact() {
        return $this->hasOne(Contact::class, 'item_content_id');
    }
    public function contacts() {
        return $this->hasMany(Contact::class, 'item_content_id');
    }

    public function dcprojects() {
        return $this->hasMany('App\Models\DCProject', 'content_id');
    }
    public function dcproject() {
        return $this->hasOne('App\Models\DCProject', 'content_id');
        // return $this->dcprojects()->first();
    }

    
    /* 
     * Scout/Algolia indexing 
     */
    public function shouldBeSearchable()
    {
        return $this->item && $this->item->status == 'published' && 
            (
                $this->item->type == 'article' 
                || $this->item->type == 'static' 
                || $this->item->type == 'library' 
                || $this->item->subtype == 'file'
            );
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();

        unset($array['item']);
        $array['content'] = \App\Actions\CleanHtml::clean($array['content']);
        $array['created_at'] = $this->item->created_at->format('Y-m-d');
        $array['updated_at'] = $this->item->updated_at->format('Y-m-d');
        $array['publish_at'] = $this->item->publish_at->format('Y-m-d');
        // $array['collections'] = array_map(fn($coll) => $coll->content $this->item->collections;
        $lang = $this->lang;
        $colls = \App\Actions\PrimaryCollection::all($this->item->collections(), $lang);
        $collTitles = [];
        foreach ($colls as $coll) {
            $collTitles[] = $coll->content->title;
        }
        $array['collections'] = $collTitles;
        return $array;
    }

    protected function makeAllSearchableUsing($query)
    {
        // return $query->with('author');
        // search only published items
        // return $query->with(['item' => ]); 
        return $query
            ->withoutGlobalScopes()
            ->with([
                'item', 
                'item.collections',
                'item.collections.contents' 
                    => fn($q) => $q->select('id','lang','title')
                                ->where('lang', 'item_contents.lang')
                                ->withoutGlobalScopes()
            ]);
    }
}
