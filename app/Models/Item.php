<?php

namespace App\Models;

use App;
use App\Scopes\PublishedScope;
use Illuminate\Support\Carbon;
use OwenIt\Auditing\Contracts\Auditable;

class Item extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $dates = [
        'publish_at',
    ];
    // protected $with = ['content', 'images'];
    // protected $withCount = ['contents', 'images'];
    protected $withCount = ['authors'];

    public function getPublishAtAttribute($date) {
        if ($date) {
            return new Carbon($date);
        } else {
            return $this->created_at;
        }
    }

    protected static function booted() {
        static::addGlobalScope(new PublishedScope);
    }

    public function isPublished() {
        return $this->status == 'published' 
            && (!$this->publish_at || $this->publish_at < Carbon::now());
    }

    public function content() {
        // $locale = App::getLocale() ?? 'en';
        return $this->hasOne('App\Models\ItemContent');//->whereIn('lang', [$locale, '*']);
    }

    public function contents() {
        return $this->hasMany('App\Models\ItemContent')
            ->withoutGlobalScopes()
            ->orderBy('lang')
            ;
    }
    
    public function images() {
    	return $this->belongsToMany('App\Models\Item', 'item_imageitems', 'item_id', 'imageitem_id')
            ->where('subtype', 'image')
            ->withPivot('order')
    		->orderBy('order')
            ;
    }

    public function icons() {
    	return $this->belongsToMany('App\Models\Item', 'item_imageitems', 'item_id', 'imageitem_id')
            ->where('subtype', 'image.icon')
    		->withPivot('order')
    		->orderBy('order')
            ;
    }
    public function icon() {
        return $this->icons()->first();
    }

    public function portraitImages() {
    	return $this->belongsToMany('App\Models\Item', 'item_imageitems', 'item_id', 'imageitem_id')
            ->where('subtype', 'image.portrait')
    		->withPivot('order')
    		->orderBy('order')
            ;
    }
    public function squareImages() {
    	return $this->belongsToMany('App\Models\Item', 'item_imageitems', 'item_id', 'imageitem_id')
            ->where('subtype', 'image.square')
    		->withPivot('order')
    		->orderBy('order')
            ;
    }

    public function allImages() {
    	return $this->belongsToMany('App\Models\Item', 'item_imageitems', 'item_id', 'imageitem_id')
            ->withPivot('order')
    		->orderBy('order')
            ;
    }

    public function imageForItems() {
        return $this->belongsToMany('App\Models\Item', 'item_imageitems', 'imageitem_id', 'item_id');
    }

    public function imageForCollections() {
        return $this->belongsToMany('App\Models\Collection', 'collection_imageitems', 'imageitem_id', 'collection_id');
    }

    public function collections() {
        return $this->belongsToMany('App\Models\Collection', 'item_collection')
            // ->withCount('items')
            // ->withPivot('order', 'item_order')
            ->withPivot('order')
                ->withTimestamps()
            ->orderBy('order')
            ;
    }

    public function collectionsOfType($type) {
        return $this->collections->where('type', $type);
    }

    protected static $CollectionMapping = [
        'articles' => 'articles',
        'library' => 'library',
        'structure' => 'structure',
        'author' => 'persons',
        'tag' => 'tag',
        'theme' => 'tag',
        'region' => 'geo',
        'country' => 'geo',
        'persons' => 'persons',
        'contacts' => 'structure',
        'listing' => 'structure',
        'dossier' => 'dossier',
        'dossier_sub' => 'dossier',
        'workarea' => 'workarea',
        'sdi' => 'workarea',
        'sdi_group' => 'workarea'
    ];

    public function collectionsByGroup() {
        // $groupedCollections = $this->collections->groupBy('type');
        $groups = collect(['workarea' => [], 'dossier' => [], 'geo' => []]);
        foreach ($this->collections as $collection) {
            $mapped = isset(self::$CollectionMapping[$collection->type]) 
                        ? self::$CollectionMapping[$collection->type] 
                        : 'structure';
            if (!$groups->has($mapped)) {
                $groups->put($mapped, []);
            }
            $mappedColl = $groups->get($mapped);
            $mappedColl[] = $collection;
            $groups->put($mapped, $mappedColl);
        }
        // $groups = array_filter($groups, fn($group) => count($group) > 0);
        $groups = $groups->filter(function($group, $name) {
            if ($name == 'tag') {
                return false;
            }
            return count($group) > 0;
        });
        return $groups;
    }

    public function relatedItems() {
        // related collections
        $items = [];
        $ids = [$this->id];
        foreach ($this->collections as $coll) {
            $item = $coll->items()
                        ->whereIn('type', ['article', 'static'])
                        ->whereNotIn('items.id', $ids)
                        ->first()
                        ;
            if (!$item) {
                continue;
            }
            // $items[] = $item;
            $ids[] = $item->id;
            if (count($ids) > 6) {
                break;
            }
        }
        $ids = array_slice($ids, 1);
        $items = Item::whereIn('id', $ids)
                    ->with([
                        'content:id,title,slug,item_id,lang',
                        'images.content.images',
                        'collections',
                        'collections.content:id,title,slug,collection_id,lang'
                    ])
                    ->orderByRaw('COALESCE(publish_at, created_at) desc')
                    ->get()
                    ;
        return $items;
    }

    public function authors() {
        return $this->collections()->where('type', 'author');
    }

    public function attachmentGroups() {
        // $locale = App::getLocale();
        return $this->hasMany('App\Models\AttachmentGroup')
                // ->where(function($q) use ($locale) {
                //     $q->whereIn('lang', [$locale, '*'])
                //       ->orWhereNull('lang');
                // })
                ->orderBy('order');
    }

    public function affiliate() {
        return $this->hasOne('App\Models\Affiliate');
    }
    
    public function member() {
    	return $this->hasOne('App\Models\Member');
    }

    public function isOpinion() {
        return $this->collections()
            ->where('collections.id', config('eiie.collection.opinion', 0))
            ->count() > 0;
    }

}
