<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Http\Request;

use Spatie\Menu\Laravel\Menu;
use Spatie\Menu\Laravel\Link;

use App\Models\Collection;

class PrimaryNavigation extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
		$priorities = Collection::with([
							'content',
							'subCollections',
							'subCollections.content'
						])
						->firstWhere('id', config('eiie.collection.priorities'))
						;
		if ($priorities) {
			$priorities = $priorities->subCollections;
		} else {
			$priorities = [];
		}

		$campaignsColl = Collection::with([
							'content',
							'subCollections',
							'subCollections.content'
						])
						->firstWhere('id', config('eiie.collection.campaigns'))
						;
		$campaigns = isset($campaignsColl->subCollections[0]) 
						? $campaignsColl->subCollections->take(4)
						: []
						;

		$spotlight = Collection::with([
						'content',
						'subCollections',
						'subCollections.content'
					])
					->firstWhere('id', config('eiie.collection.spotlight'))
					;
		if ($spotlight) {
			$spotlight = $spotlight->subCollections->take(4);
		} else {
			$spotlight = [];
		}

		$requestSegments = request()->segments();
		
		/* https://docs.spatie.be/menu */
        $menu = Menu::new()
			->addClass('nav_main')
            ->add(Menu::new()
				->prepend('<span class="nav_main_title">'.__('eiie.About EI').'</span>')
				->addParentClass('nav_main_item nav_two')
				->addClass('nav_drop nav_two_sub')
				->add(Menu::new()
					->prepend('<h2>'.__('eiie.About EI').'</h2>')
					->addParentClass('nav_drop_section')
                    ->addClass('nav_drop_listing')
					->route('about.who-we-are', __('eiie.Who we are'))
					->route('about.origins-and-history', __('eiie.Origins and history'))
					->route('affiliates', __('eiie.Our Members'))
					->route('about.leaders', __('eiie.Our Leaders'))
					// ->route('about.team', __('eiie.Our Team'))
					->route('about.jobs', __('eiie.Career Opportunities'))
					->route('regions', __('eiie.Regions and Countries'))
					->route('governance', __('eiie.Governance'))
					->route('about.global-unions', __('eiie.Global Unions'))
					->route('contact.newsletter', __('eiie.Subscribe to Our Newsletters'))
					->route('contact.ei-ie', __('eiie.Contact Us'))
					// ->route('about.principal-aims', __('eiie.Principal Aims'))
					// ->route('about.staff-profiles', __('eiie.Staff Profiles'))
					// ->route('about.jobs', __('eiie.Jobs'))
					// ->route('about.call-for-proposals', __('eiie.Call for Proposals'))
                )
                // ->add(Menu::new()
				// 	->prepend('<h2>'.__('eiie.Governance').'</h2>')
				// 	->addParentClass('nav_drop_section')
				// 	->addClass('nav_drop_listing')
				// 	->route('governance.constitution-and-bylaws', __('eiie.Constitution and Bylaws'))
				// 	->route('governance.world-congress', __('eiie.World Congress'))
				// 	->route('governance.declarations-and-statements', __('eiie.Declarations and Statements'))
				// 	->route('governance.executive-board', __('eiie.Executive Board'))
				// 	->route('governance.ei-regional-committees', __('eiie.EI Regional Committees'))
				// 	->route('governance.annual-reports', __('eiie.Annual Reports'))
				// 	->route('governance.regions', __('eiie.Regions'))
                // )
                // ->add(Menu::new()
				// 	->prepend('<h2>'.__('eiie.Contact').'</h2>')
				// 	->addParentClass('nav_drop_section')
				// 	->addClass('nav_drop_listing')
				// 	->route('contact.ei-ie', __('eiie.Contact us'))
				// 	->route('contact.members', __('eiie.Find an EI affiliate'))
				// 	->route('contact.legal-notice', __('eiie.Legal Notice'))
				// 	->route('contact.data-protection-policy', __('eiie.Data Protection Policy'))
                // )
			)
            ->add(Menu::new()
				->prepend('<span class="nav_main_title">'.__('eiie.Our Work').'</span>')
				->addParentClass('nav_main_item nav_one')
				->addClass('nav_drop nav_one_sub')
            	->add(Menu::new()
					->prepend('<h2>'.__('eiie.Latest').'</h2>')
					->addParentClass('nav_drop_section')
					->addClass('nav_drop_listing')
					->route('news', __('eiie.News'))
					->route('opinion', __('eiie.Opinion'))
					->route('statements', __('eiie.Statements'))
					->route('take-action', __('eiie.Take action!'))
				)
				->add(Menu::new()
					->prepend('<h2>'.__('eiie.Our Priorities').'</h2>')
					->addParentClass('nav_drop_section')
					->addClass('nav_drop_listing')
					->fill($priorities, function($menu, $workarea) {
							if (isset($workarea->content)) {
								$menu->route('workarea', $workarea->content->title, [
									'id' => $workarea->id, 
									'slug' => $workarea->content->slug
								]);
							}
						})
				)
				// ->add(Link::toRoute('spotlight', __('eiie.Spotlight')))
                ->add(Menu::new()
					->prepend('<h2>'.__('eiie.Spotlight').'</h2>')
					->addParentClass('nav_drop_section')
					->addClass('nav_drop_listing')
					->fill($spotlight, function($menu, $dossier) {
							if (isset($dossier->content)) {
								$menu->route('dossier', $dossier->content->title, [
									'id' => $dossier->id,	
									'slug' => $dossier->content->slug
								]);
							}
						})
					->add(Link::toRoute('spotlight', __('eiie.Show all'))
							->addParentClass('nav_drop_listing_footer'))
				)
				// ->add(Menu::new()
				// 	->prepend('<h2>'.__('eiie.Country Profiles').'</h2>')
				// 	->addParentClass('nav_drop_section')
				// 	->addClass('nav_drop_listing')
					// ->fill($regions, function($menu, $region) {
					// 		if (isset($region->content)) {
					// 			$menu->route('region', $region->content->title, [
					// 				'id' => $region->id, 
					// 				'slug' => $region->content->slug
					// 			]);
					// 		}
					// 	})
				// )
			)
            ->addIf($campaignsColl, Menu::new()
				->prepend('<span class="nav_main_title">'.__('eiie.Campaigns').'</span>')
				->addParentClass('nav_main_item nav_three')
				->addClass('nav_drop nav_three_sub')
				->add(Menu::new()
					->prepend('<h2>'.__('eiie.Campaigns').'</h2>')
					->addParentClass('nav_drop_section')
					->addClass('nav_drop_listing')
					->fill($campaigns, function($menu, $campaign) {
						if (isset($campaign->content)) {
							$menu->route('dossier', $campaign->content->title, [
								'id' => $campaign->id, 
								'slug' => $campaign->content->slug
							]);
						}
					})
					->add(Link::toRoute('campaigns', __('eiie.Show all'))
							->addParentClass('nav_drop_listing_footer'))
				)
			)
            ->add(Menu::new()
				->prepend('<span class="nav_main_title">'.__('eiie.Resources').'</span>')
				->addParentClass('nav_main_item nav_three')
				->addClass('nav_drop nav_three_sub')
            	->add(Menu::new()
					->prepend('<h2>'.__('eiie.Resources').'</h2>')
					->addParentClass('nav_drop_section')
					->addClass('nav_drop_listing')
					->route('resources.publications-and-research', 
							__('eiie.Publications and Research'))
					->route('resources.policy-briefs', 
							__('eiie.Policy briefs'))
					->route('resources.world-congress-resolutions', 
							__('eiie.World Congress resolutions'))
					// ->route('resources.podcasts', 
					// 		__('eiie.Podcasts'))
					->route('resources.videos', 
							__('eiie.Videos'))
					->route('authors', 
							__('eiie.Authors'))
					->route('resources.dcprojects', 
							__('eiie.Development Cooperation Projects'))							
					// ->route('resources', __('eiie.Links'), ['subtype' => 'link'])
					// ->route('resources', __('eiie.Images'), ['subtype' => 'image'])
					// ->route('resources', __('eiie.Files'), ['subtype' => 'file'])
					// ->add(Link::toRoute('resources', __('eiie.All Resources'))
					// 		->addParentClass('nav_drop_listing_footer'))
				)
            	// ->add(Menu::new()
				// 	->prepend('<h2>'.__('eiie.Authors').'</h2>')
				// 	->addParentClass('nav_drop_section')
				// 	->addClass('nav_drop_listing')
				// 	->route('authors', __('eiie.All Authors'))
				// )
			)
			// ->add(Menu::new()
			// 	->addParentClass('nav_search_mobile')
			// 	->fill(
			// 		config('app.locales'), 
			// 		function($menu, $locale, $localeKey) {
			// 			$segs = request()->segments();
			// 			$segs[0] = $localeKey;
			// 			$menu->add(
			// 				Link::to('/'.implode('/', $segs), $locale)
			// 					->addParentClass(\App::isLocale($localeKey) ? 'active' : '')
			// 			);
			// 		}
			// 	)
			// )
			->setActiveFromRequest()
		;

        return $menu;
        // return view('components.menu');
    }
}