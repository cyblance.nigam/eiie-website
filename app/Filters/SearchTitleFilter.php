<?php 

namespace App\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class SearchTitleFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $locale = \App::getLocale() ?? 'en';

        $query->whereHas('contents', function ($q) use ($value, $locale) {
            $q->whereIn('lang', [$locale, '*'])
                ->where('title', 'like', "%$value%")
                ;
        });
    }
}
