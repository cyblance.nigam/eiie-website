<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Collection;
use Illuminate\Http\Request;
use Mail;
use App\Mail\ContactUs;

class ContactController extends Controller {

    /**
     */
    public function show() {
        $offices = Collection::find(config('eiie.collection.contact-offices', 0))
        if ($offices) {
            $offices = $offices->items
        }
        return view('contactus', ['offices' => $offices]);
    }

    public function contactPost(Request $request){
        $this->validate($request, [
                        // 'first_name' => 'string',
                        'email' => 'required|email',
                        'message' => 'required'
                ]);
        if ($request->isRobot()) {
            // return back()->with('success', __('eiie.contact_us_fail'));
            abort(500);
        }
        $name = trim($request->get('first_name').' '.$request->get('last_name'));
        $email = $request->get('email');
        $subject = trim('Website contact '.$request->get('subject'));
        $mailData = $request->only('email', 'phone');
        $mailData['name'] = $name;
        $mailData['subject'] = trim('Website contact '.$request->get('subject'));
        $mailData['mailMessage'] = $request->get('message');

        Mail::to(config('EIIE_CONTACT_FORM_RCPT', 'headoffice@ei-ie.org'))
            ->send(new ContactUs($mailData));        

        return back()->with('success', __('eiie.contact_us_thanks'));
    }


}
