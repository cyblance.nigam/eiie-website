<?php

namespace App\Http\Controllers;

use App\Http\Resources\CountryResource;
use App\Models\Item;
use App\Models\ItemContent;
use App\Models\Collection;
use App\Sorts\CollectionContentTitleSort;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class ApiController extends Controller 
{

    public function listAffiliates(Request $request)
    {
        // $searchAffiliate = $request->input('filter.affiliate');
        // $affiliatePlaceholderCollection = Collection::find(config('eiie.collection.affiliates'));
        
        // $locale = \App::getLocale() ?? 'en';

        $titleSort = AllowedSort::custom('title', new CollectionContentTitleSort(), 'title');
        $countries = QueryBuilder::for(Collection::class)
            ->where('type', 'country')
            ->whereHas('items', fn($query) => $query->where('type', 'affiliate'))
            ->allowedFilters([
                AllowedFilter::partial('country', 'content.title'),
                AllowedFilter::exact('region_id', 'parentCollections.id'),
                // AllowedFilter::custom('affiliate', new SearchTitleFilter),
                AllowedFilter::callback('affiliate', fn($query, $search) => 
                    $query->whereHas('items', fn($query) =>
                        $query->where('type', 'affiliate')
                            ->where(fn($query) =>
                                $query->whereHas('content', fn($query) =>
                                    $query->where('title', 'like', "%$search%")
                                )->orWhereHas('affiliate', fn($query) => 
                                    $query->where('acronym', 'like', "%$search%")
                                )
                            )
                    )
                )
            ])
            ->allowedSorts([$titleSort])
            ->defaultSort($titleSort)
            ->with([
                'items' => function($query) {
                    $query->without([
                        'collections',
                        'images',
                    ])
                    ->with([
                        'content:id,item_id,title'
                    ])
                    ;
                    $query->where('type', 'affiliate');
                    // if ($searchAffiliate) {
                    //     $query->where(fn($query) => 
                    //         $query->whereHas('content', fn($query) =>
                    //             $query->where('title', 'like' , "%$searchAffiliate%")
                    //         )->orWhereHas('affiliate', fn($query) => 
                    //             $query->where('acronym', 'like', "%$searchAffiliate%")
                    //         )
                    //     );
                    // }
                    // remove the default country ordering (which is by date)
                    $query->reorder(); 
                    // and apply alphabetical ordering
                    $query->orderBy(ItemContent::select('title')
                        ->whereColumn('item_id', 'items.id')
                        ->orderBy('title', 'asc')
                        ->limit(1)
                    );
                },
                'items.affiliate',
                'content:id,collection_id,lang,title',
            ])
            ->without([                
                'items.content',
            ])
            ->get()
            ;
        
        return CountryResource::collection($countries);
    }

}