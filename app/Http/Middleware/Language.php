<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $locales = config('app.locales');

        // Check if the first segment matches a language code
        if (!array_key_exists($request->segment(1), $locales)) {
            // Store segments in array
            $segments = $request->segments();

            if ($request->segment(1) == 'spa') {
                $segments[0] = 'es'; // hardcoded redirect from spa to es
            } else {
                // Set the default language code as the first segment
                $segments = \Arr::prepend($segments, config('app.fallback_locale'));
            }
            // Redirect to the correct url
            return redirect()->to(implode('/', $segments));
        } else {
            App::setLocale($request->segment(1));
        }
        
        return $next($request);
    }
}
