<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // UserSeeder::class,
            // CollectionSeeder::class,
            // AuthorMigration::class,
            // MediaMigration::class,
            // ResourceMigration::class,
            // TagsMigration::class,
            // CountryMigration::class,
            // CountrySectionsMigration::class,
            // PostMigration::class,
            // AffiliateMigration::class,
            // DossierMigration::class,
            // WorkAreaMapper::class,
            // BlogSeriesMigration::class,
            // Featured::class,
            // ContactsImport::class,
            // MergeCollections::class,
            // Reorganize::class,
            // ClimateCampaign::class,
        ]);
    }
}

