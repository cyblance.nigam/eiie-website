import React from "react";

import { GridColDef, GridValueFormatterParams } from "@mui/x-data-grid";
import dayjs from "dayjs";

import { Collection, Content, Item } from "../Models";
import { GridCellParams } from "@mui/x-data-grid";

export const makeDateColumn = (field: string): GridColDef => ({
    field,
    flex: 1,
    valueFormatter: (params: GridValueFormatterParams) =>
        dayjs(params.value as string).format("YYYY-MM-DD HH:mm"),
});

export const LangColumn: GridColDef = {
    field: "lang",
    sortable: false,
    renderCell: (params: GridCellParams) => (
        <>
            {(params.row as Item | Collection).contents
                .map((content: Content) => content.lang)
                .join(",")}
        </>
    ),
};
