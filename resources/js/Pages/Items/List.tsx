import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import { Tooltip } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import { GridColDef } from "@mui/x-data-grid";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import route from "ziggy-js";

import PublishStatusIcon from "../../Components/General/PublishStatusIcon";
import Listing from "../../Components/Listing";
import useDataSource from "../../Components/useDataSource";
import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import { Collection } from "../../Models";
import { findTitle } from "../../Models/Content";
import { AuthPageProps, IListingPageProps } from "../../Models/Inertia";
import Item from "../../Models/Item";
import Paginated from "../../Models/Paginated";
import { LangColumn, makeDateColumn } from "../../Utils/DataGridUtils";

interface IProps extends IListingPageProps, AuthPageProps {
    items: Paginated<Item>;
    collection?: Collection;
}
const List: React.FC<IProps> = ({
    items,
    collection,
    filter: _filter,
    sort,
    can,
}) => {
    const [search, setSearch] = useState<string>();
    const [filter, setFilter] = useState(_filter);
    const dataSource = useDataSource({
        mode: "inertia",
        paginatedData: items,
        search,
        filter,
        sort,
    });

    const columns: GridColDef[] = [
        {
            field: "id",
            renderCell: (cell) => (
                <>
                    {cell.row.id} <PublishStatusIcon item={cell.row as Item} />
                </>
            ),
        },
        {
            field: "title",
            renderCell: (cell) => (
                <InertiaLink
                    href={route("admin.items.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    {findTitle(cell.row as Item) ||
                        (cell.row as Item).contents[0]?.title ||
                        "- no title -"}
                </InertiaLink>
            ),
            flex: 2,
        },
        LangColumn,
        makeDateColumn("created_at"),
        makeDateColumn("updated_at"),
        { field: "type" },
        { field: "subtype" },
    ];

    const onCreate = () => {
        Inertia.get(route("admin.items.create"));
    };

    useEffect(() => {
        setFilter(_filter);
    }, [_filter]);

    const title = collection ? `Items in '${findTitle(collection)}'` : `Items`;

    return (
        <>
            <AppBarHeader
                title={title}
                afterTitle={
                    collection?.id ? (
                        <Tooltip title="Edit collection">
                            <IconButton
                                size="small"
                                onClick={() =>
                                    Inertia.get(
                                        route("admin.collections.edit", {
                                            id: collection.id,
                                        })
                                    )
                                }
                                color="inherit"
                            >
                                <EditIcon />
                            </IconButton>
                        </Tooltip>
                    ) : undefined
                }
                filterOptions={[
                    {
                        label: "Missing translations",
                        name: "missing_translations",
                    },
                    {
                        label: "Missing workarea",
                        name: "missing_workarea",
                    },
                    // ...(filter?.filter.subtype === "file" ||
                    // filter?.filter.subtype?.startsWith("image")
                    //     ? [
                    //           {
                    //               label: "Missing files",
                    //               name: "missing_files",
                    //           },
                    //       ]
                    //     : []),
                    {
                        label: "Hide unpublished",
                        name: "status",
                        value: "published",
                    },
                ]}
                filter={filter}
                onSearch={setSearch}
                onChangeFilter={setFilter}
            >
                <IconButton
                    onClick={onCreate}
                    disabled={can ? !can?.items?.create : true}
                    size="large"
                >
                    <AddIcon />
                </IconButton>
            </AppBarHeader>
            <ContentScroll>
                <Listing columns={columns} dataSource={dataSource}></Listing>
            </ContentScroll>
        </>
    );
};

export default List;
