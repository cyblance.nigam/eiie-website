import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import { Box, IconButton } from "@mui/material";
// import Switch from "@mui/material/Switch";
import { GridColDef } from "@mui/x-data-grid";
import AddIcon from "@mui/icons-material/Add";
import { makeStyles } from "@mui/styles";
import route from "ziggy-js";

import LinkQuery from "../../Components/General/LinkQuery";
import PublishStatusIcon from "../../Components/General/PublishStatusIcon";
import Listing, { DataCellCollectionTree } from "../../Components/Listing";
import useDataSource from "../../Components/useDataSource";
import { StatusType } from "../../Config";
import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import Collection from "../../Models/Collection";
import { findTitle } from "../../Models/Content";
import { AuthPageProps, IListingPageProps } from "../../Models/Inertia";
import Paginated from "../../Models/Paginated";
import { LangColumn, makeDateColumn } from "../../Utils/DataGridUtils";

const useStyles = makeStyles(() => ({
    cellRightAlign: {
        display: "block !important",
    },
}));

interface IProps extends IListingPageProps, AuthPageProps {
    collections: Paginated<Collection>;
    parent_types: string[];
    child_types: string[];
}
const List: React.FC<IProps> = ({
    collections,
    child_types,
    filter: _filter,
    sort,
    can,
}) => {
    const classes = useStyles();
    const [filter, setFilter] = useState(_filter);
    const [search, setSearch] = useState<string>();
    const dataSource = useDataSource({
        mode: "inertia",
        paginatedData: collections,
        search,
        filter,
        sort,
    });
    // const { parse, format, formats } = useUtils<AdapterDayjs>();
    // console.log(formats);

    // const [statuses, setStatuses] = useState<{
    //     [key: number]: StatusType;
    // }>({});

    useEffect(() => {
        const newStatuses: { [key: number]: StatusType } = {};
        collections.data.forEach(({ id, status }) => {
            newStatuses[id] = status;
        });
        // setStatuses(newStatuses);
    }, [collections]);

    // const toggleStatus = (id: number) => {
    //     setStatuses((statuses) => ({
    //         ...statuses,
    //         [id]: statuses[id] === "published" ? "unpublished" : "published",
    //     }));
    //     console.log("ToDo toggle publish", id);
    // };

    const onCreate = () => {
        Inertia.get(route("admin.collections.create"));
    };

    useEffect(() => {
        setFilter(_filter);
    }, [_filter]);

    const columns: GridColDef[] = [
        {
            field: "id",
            renderCell: (cell) => (
                <Box display="flex" justifyContent="space-between">
                    {cell.id}{" "}
                    <DataCellCollectionTree
                        collection={cell.row as Collection}
                        onSelect={(subCollection) =>
                            Inertia.get(
                                route("admin.collections.edit", {
                                    id: subCollection.id,
                                })
                            )
                        }
                    />
                </Box>
            ),
            align: "right",
        },
        /*
        {
            field: "status",
            headerName: "Published",
            disableClickEventBubbling: true,
            renderCell: (cell) => (
                <Switch
                    checked={
                        statuses[(cell.row as Collection).id] === "published"
                    }
                    color="primary"
                    onChange={() => {
                        toggleStatus((cell.row as Collection).id);
                    }}
                />
            ),
        },
        */
        {
            field: "title",
            renderCell: (cell) => (
                <>
                    {child_types?.includes((cell.row as Collection).type) ? (
                        <>&mdash;&nbsp;&nbsp;</>
                    ) : (
                        <></>
                    )}
                    <InertiaLink
                        href={route("admin.collections.edit", {
                            id: cell.row.id,
                        }).toString()}
                    >
                        {<PublishStatusIcon item={cell.row as Collection} />}{" "}
                        {findTitle(cell.row as Collection) ?? "- no title -"}
                    </InertiaLink>
                </>
            ),
            flex: 2,
        },
        LangColumn,
        makeDateColumn("created_at"),
        makeDateColumn("updated_at"),
        {
            field: "items_count",
            headerName: "Items",
            renderCell: (cell) => (
                <LinkQuery
                    toRoute="admin.items.index"
                    query={{ filter: { ["collection.id"]: cell.row.id } }}
                >
                    {cell.value}
                </LinkQuery>
            ),
            cellClassName: () => classes.cellRightAlign,
            align: "right",
        },
        { field: "type" },
        { field: "layout" },
    ];

    const filterOptions = [
        {
            label: "Missing translations",
            name: "missing_translations",
        },
        {
            label: "Empty collections",
            name: "items_count",
            value: "0",
        },
        {
            label: "Hide unpublished",
            name: "status",
            value: "published",
        },
    ];
    if (filter?.filter?.type?.includes("dossier")) {
        (filterOptions as any).push({
            label: "Show subdossiers",
            name: "type",
            value: "dossier,dossier_sub",
            disabledValue: "dossier",
        });
    }

    return (
        <>
            <AppBarHeader
                title="Collections"
                filterOptions={filterOptions}
                filter={filter}
                onSearch={setSearch}
                onChangeFilter={setFilter}
            >
                <IconButton
                    onClick={onCreate}
                    disabled={
                        !can?.collections?.create &&
                        !can?.collections?.createLimited
                    }
                    size="large"
                >
                    <AddIcon />
                </IconButton>
            </AppBarHeader>
            <ContentScroll>
                <Listing columns={columns} dataSource={dataSource}></Listing>
            </ContentScroll>
        </>
    );
};

export default List;
