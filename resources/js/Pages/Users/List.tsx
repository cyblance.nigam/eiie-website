import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import IconButton from "@mui/material/IconButton";
import { GridColDef } from "@mui/x-data-grid";
import AddIcon from "@mui/icons-material/Add";
import route from "ziggy-js";

import Listing from "../../Components/Listing";
import useDataSource from "../../Components/useDataSource";
import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import { User } from "../../Models";
import { AuthPageProps, IListingPageProps } from "../../Models/Inertia";
import Paginated from "../../Models/Paginated";
import { makeDateColumn } from "../../Utils/DataGridUtils";
import { GridCellParams } from "@mui/x-data-grid";

interface IProps extends IListingPageProps, AuthPageProps {
    users: Paginated<User>;
}
const List: React.FC<IProps> = ({ users, filter: _filter, sort, can }) => {
    const [search, setSearch] = useState<string>();
    const [filter, setFilter] = useState(_filter);
    const dataSource = useDataSource({
        mode: "inertia",
        paginatedData: users,
        search,
        filter,
        sort,
    });

    const columns: GridColDef[] = [
        { field: "id" },
        {
            field: "name",
            renderCell: (cell: GridCellParams) => (
                <InertiaLink
                    href={route("admin.users.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    {(cell.row as User).name ?? "- no name -"}
                </InertiaLink>
            ),
            flex: 2,
        },
        {
            field: "email",
            renderCell: (cell: GridCellParams) => (
                <InertiaLink
                    href={route("admin.users.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    {(cell.row as User).email ?? "- no email -"}
                </InertiaLink>
            ),
            flex: 2,
        },
        { field: "role" },
        makeDateColumn("created_at"),
        makeDateColumn("updated_at"),
    ];

    const onCreate = () => {
        Inertia.get(route("admin.users.create"));
    };

    useEffect(() => {
        setFilter(_filter);
    }, [_filter]);

    const title = "Users";

    return (
        <>
            <AppBarHeader
                title={title}
                filterOptions={[]}
                filter={filter}
                onSearch={setSearch}
                onChangeFilter={setFilter}
            >
                <IconButton
                    onClick={onCreate}
                    disabled={!can?.users?.create}
                    size="large"
                >
                    <AddIcon />
                </IconButton>
            </AppBarHeader>
            <ContentScroll>
                <Listing columns={columns} dataSource={dataSource}></Listing>
            </ContentScroll>
        </>
    );
};

export default List;
