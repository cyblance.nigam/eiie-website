import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import {
    Box,
    Button,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Paper,
    Select,
    TextField,
    Typography,
    FormHelperText,
    Theme,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import route from "ziggy-js";

import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import { ErrorPageProps } from "../../Models";

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        paddingTop: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
    },
}));

interface IProps {
    roles: { [key: string]: string };
}
const Create: React.FC<IProps & ErrorPageProps> = ({ roles, errors }) => {
    const classes = useStyles();
    const [name, setName] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [role, setRole] = useState<string>("");
    const [isValid, setIsValid] = useState(false);

    const onCreate = () => {
        const params: any = { name, email, role };
        Inertia.post(route("admin.users.store").toString(), params);
    };

    useEffect(() => {
        if (Object.entries(roles).length === 1) {
            setRole(Object.entries(roles)[0][0]);
        }
    }, [roles]);

    useEffect(() => {
        setIsValid(Boolean(role) && Boolean(name) && Boolean(email));
    }, [role, name, email]);

    return (
        <>
            <AppBarHeader title="Create User" />
            <ContentScroll>
                <Box className={classes.container}>
                    <Paper className={classes.container}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Typography variant="body1">
                                    Provide the required information. The
                                    password will be set by the user via a link
                                    sent to their email address.
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    label="Name"
                                    variant="outlined"
                                    fullWidth
                                    helperText={errors?.name}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    label="Email address"
                                    variant="outlined"
                                    fullWidth
                                    helperText={errors?.email}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <FormControl variant="outlined" fullWidth>
                                    <InputLabel>Role</InputLabel>
                                    <Select
                                        value={role}
                                        onChange={(e) =>
                                            setRole(e.target.value as string)
                                        }
                                        label="Type"
                                        fullWidth
                                    >
                                        {Object.entries(roles).map(
                                            ([role, label], i) => (
                                                <MenuItem value={role} key={i}>
                                                    {label}
                                                </MenuItem>
                                            )
                                        )}
                                    </Select>
                                    <FormHelperText>
                                        {errors?.role}
                                    </FormHelperText>
                                </FormControl>
                            </Grid>

                            <Grid item xs={12}>
                                <Button
                                    size="large"
                                    variant="contained"
                                    color="primary"
                                    onClick={onCreate}
                                    disabled={!isValid}
                                >
                                    Create
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Box>
            </ContentScroll>
        </>
    );
};

export default Create;
