import { Theme } from "@mui/material";
import { createStyles, makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
        },
        formbox: {
            display: "flex",
            width: "30%",
            minWidth: 300,
            flexDirection: "column",
            justifyContent: "center",
            padding: theme.spacing(2),

            "& > *": {
                marginBottom: `${theme.spacing(2)} !important`,
                "&:last-child": {
                    marginBottom: `0 !important`,
                },
            },
        },
        feedback: {
            // minHeight: "3em",
        },
        field: {
            marginTop: theme.spacing(2),
            marginBottom: theme.spacing(2),
        },
        button: {
            marginTop: theme.spacing(2),
        },
    })
);

export default useStyles;
