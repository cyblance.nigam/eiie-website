import React, { KeyboardEventHandler, useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { Box, Button, Paper, Typography, TextField } from "@mui/material";
import route from "ziggy-js";

import { FlashMessages } from "../../Layout";
import { ErrorPageProps } from "../../Models";

import useStyles from "./Login.styles";

const PasswordForgot: React.FC<ErrorPageProps> = ({ errors }) => {
    const styles = useStyles();
    const [feedback, setFeedback] = useState<string>();
    const [email, setEmail] = useState("");

    useEffect(() => {
        if (errors?.error) {
            setFeedback(errors.error);
        }
    }, [errors]);

    const doReset = () => {
        Inertia.post(route("password.email"), {
            email,
        });
    };

    const onKeyPress: KeyboardEventHandler<HTMLElement> = (e) => {
        if (!email) {
            return;
        }
        if (e.key === "Enter") {
            doReset();
        }
    };

    return (
        <form>
            <FlashMessages />
            <Box className={styles.container}>
                <Paper className={styles.formbox}>
                    <Typography variant="h4">Forgot password</Typography>
                    <Typography variant="body1">
                        Reset your password by providing your email address, a
                        reset link will be sent there.
                    </Typography>
                    <Typography variant="body1" className={styles.feedback}>
                        {feedback}
                    </Typography>
                    <TextField
                        label="Email address"
                        value={email}
                        variant="outlined"
                        type="email"
                        className={styles.field}
                        onChange={(e) => setEmail(e.target.value)}
                        onKeyPress={onKeyPress}
                        helperText={errors?.email || " "}
                        autoComplete="username"
                        error={Boolean(errors?.email)}
                    />
                    <Button
                        variant="contained"
                        size="large"
                        className={styles.button}
                        onClick={doReset}
                        disabled={email.length === 0}
                    >
                        Reset password
                    </Button>
                </Paper>
            </Box>
        </form>
    );
};

export default PasswordForgot;
