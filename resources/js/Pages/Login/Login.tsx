import React, { KeyboardEventHandler, useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { Box, Button, Paper, TextField, Typography } from "@mui/material";
import route from "ziggy-js";

import { FlashMessages } from "../../Layout";
import { ErrorPageProps } from "../../Models";
import useSessionExpireReload from "../../Utils/useSessionExpireReload";

import useStyles from "./Login.styles";

const Login: React.FC<ErrorPageProps> = ({ errors }) => {
    const styles = useStyles();
    const [feedback, setFeedback] = useState<string>();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [showPassword] = useState(false);

    useSessionExpireReload();

    useEffect(() => {
        setFeedback(errors?.email);
    }, [errors]);

    const doLogin = () => {
        setFeedback("");
        Inertia.post(route("login.submit"), { email, password });
    };

    const onKeyPress: KeyboardEventHandler<HTMLElement> = (e) => {
        if (e.key === "Enter") {
            doLogin();
        }
    };

    return (
        <form>
            <FlashMessages />
            <Box className={styles.container}>
                <Paper className={styles.formbox}>
                    <Typography variant="h4">Login</Typography>
                    <Typography variant="body1" className={styles.feedback}>
                        {feedback}
                    </Typography>
                    <TextField
                        label="Email"
                        value={email}
                        variant="outlined"
                        className={styles.field}
                        onChange={(e) => setEmail(e.target.value)}
                        onKeyPress={onKeyPress}
                        autoComplete="username"
                        id="user-text-field"
                    />
                    <TextField
                        label="Password"
                        value={password}
                        variant="outlined"
                        type={showPassword ? "text" : "password"}
                        className={styles.field}
                        onChange={(e) => setPassword(e.target.value)}
                        onKeyPress={onKeyPress}
                        autoComplete="current-password"
                        id="password-text-field"
                    />
                    <Button
                        variant="contained"
                        size="large"
                        className={styles.button}
                        onClick={doLogin}
                        disabled={!email.length || !password.length}
                    >
                        Login
                    </Button>
                    <Button
                        variant="text"
                        onClick={() => Inertia.get(route("password.request"))}
                    >
                        Forgot password?
                    </Button>
                    <Button
                        component="a"
                        href={route("azure")}
                        color="primary"
                        variant="text"
                    >
                        Login via Azure
                    </Button>
                </Paper>
            </Box>
        </form>
    );
};

export default Login;
