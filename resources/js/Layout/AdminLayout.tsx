import React from "react";

import {
    AppBar,
    Box,
    CssBaseline,
    Divider,
    Drawer,
    IconButton,
    Theme,
    Toolbar,
    Typography,
} from "@mui/material";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import MenuIcon from "@mui/icons-material/Menu";
import { makeStyles } from "@mui/styles";
import clsx from "clsx";
import { useLocalStorage } from "react-use";

import { Message } from "../Models/Inertia";

import { AppProvider } from "./AppContext";
import FlashMessages from "./FlashMessages";
import PrimaryNavigation from "./PrimaryNavigation";
import SessionLifetime from "./SessionLifetimeAlert";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) => ({
    root: { display: "flex" },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    appBarTitle: {
        flexGrow: 1,
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: "100vh",
        overflow: "auto",
        display: "flex",
        flexDirection: "column",
    },
    container: {
        flexGrow: 1,
        position: "relative",
    },
    drawerScroller: {
        position: "absolute",
        top: theme.mixins.toolbar.minHeight,
        left: 0,
        right: 0,
        bottom: 0,
        overflowY: "scroll",
    },
    drawerPaper: {
        position: "relative",
        whiteSpace: "nowrap",
        borderRight: "1px solid rgba(0, 0, 0, .5)",
        width: drawerWidth,
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: "hidden",
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up("sm")]: {
            width: theme.spacing(9),
        },
    },
}));

interface IProps {
    errors?: Message;
    flash?: Message;
}
export const AdminLayout: React.FC<IProps> = ({ children }) => {
    const classes = useStyles();
    const [drawerOpen, setDrawerOpen] = useLocalStorage("drawerOpen", true);

    return (
        <AppProvider>
            <div className={classes.root}>
                <CssBaseline />
                <FlashMessages />
                <SessionLifetime />
                <Drawer
                    variant="permanent"
                    open={drawerOpen}
                    classes={{
                        paper: clsx(
                            classes.drawerPaper,
                            !drawerOpen && classes.drawerPaperClose
                        ),
                    }}
                >
                    <AppBar
                        position="absolute"
                        className={clsx(classes.appBar)}
                    >
                        <Toolbar>
                            {drawerOpen ? (
                                <>
                                    <IconButton
                                        aria-label="close drawer"
                                        edge="start"
                                        onClick={() => setDrawerOpen(false)}
                                        size="large">
                                        <ChevronLeftIcon />
                                    </IconButton>
                                    <Typography
                                        variant="h6"
                                        className={classes.appBarTitle}
                                    >
                                        Ei-iE
                                    </Typography>
                                </>
                            ) : (
                                <IconButton
                                    aria-label="open drawer"
                                    edge="start"
                                    onClick={() => setDrawerOpen(true)}
                                    size="large">
                                    <MenuIcon />
                                </IconButton>
                            )}
                        </Toolbar>
                    </AppBar>
                    <Box className={classes.drawerScroller}>
                        <Divider />
                        <PrimaryNavigation />
                    </Box>
                </Drawer>
                <main className={classes.content}>
                    <div className={classes.container}>{children}</div>
                </main>
            </div>
        </AppProvider>
    );
};

export default AdminLayout;
