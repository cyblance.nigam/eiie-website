import React from "react";

import { Inertia } from "@inertiajs/inertia";
import { usePage } from "@inertiajs/inertia-react";
import { Theme, Tooltip } from "@mui/material";
import Collapse from "@mui/material/Collapse";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import AnnouncementIcon from "@mui/icons-material/Announcement";
import AssignmentIcon from "@mui/icons-material/Assignment";
import ContactPhoneIcon from "@mui/icons-material/ContactPhone";
import DescriptionIcon from "@mui/icons-material/Description";
import EventNoteIcon from "@mui/icons-material/EventNote";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import FileCopyIcon from "@mui/icons-material/FileCopy";
import GroupWorkIcon from "@mui/icons-material/GroupWork";
import ImageIcon from "@mui/icons-material/Image";
import FileIcon from "@mui/icons-material/InsertDriveFile";
import LanguageIcon from "@mui/icons-material/Language";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import LinkIcon from "@mui/icons-material/Link";
import ListIcon from "@mui/icons-material/ListAlt";
import TagIcon from "@mui/icons-material/LocalOffer";
import LogoutIcon from "@mui/icons-material/Logout";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import VideoIcon from "@mui/icons-material/OndemandVideo";
import PeopleIcon from "@mui/icons-material/People";
import PriorityHighIcon from "@mui/icons-material/PriorityHigh";
import CountryIcon from "@mui/icons-material/Public";
import TimelineIcon from "@mui/icons-material/Timeline";
import ViewModuleIcon from "@mui/icons-material/ViewModule";
import { makeStyles } from "@mui/styles";
import { useLocalStorage } from "react-use";
import route from "ziggy-js";

import { AuthPageProps, UserPageProps, Page } from "../Models";

const MENU_ITEMS = {
    items: [
        {
            filter: { type: "article" },
            label: "Articles",
            Icon: EventNoteIcon,
            tooltip: "News, opinion and blog series",
        },
        { filter: { type: "static" }, label: "Pages", Icon: DescriptionIcon },
        {
            filter: { type: "person,contact" },
            label: "Persons & Contacts",
            Icon: ContactPhoneIcon,
            tooltip: "Executive board, staff and office addresses",
        },
        {
            filter: { type: "dcproject" },
            label: "DC Projects",
            Icon: GroupWorkIcon,
            tooltip: "Development Cooperation projects",
        },
        {
            filter: { type: "library" },
            label: "Library documents",
            Icon: LibraryBooksIcon,
            tooltip: "Resolutions, constitution, policy briefs",
        },
        {
            filter: { type: "resource", subtype: "file" },
            label: "Files",
            Icon: FileIcon,
        },
        {
            filter: { type: "resource", subtype: "link" },
            label: "Links",
            Icon: LinkIcon,
        },
        {
            filter: { type: "resource", subtype: "video" },
            label: "Videos",
            Icon: VideoIcon,
        },
        {
            filter: {
                type: "resource",
                subtype: "image,image.portrait,image.icon,image.square",
            },
            label: "Images",
            Icon: ImageIcon,
        },
        { filter: {}, label: "All items" },
    ],
    collections: [
        {
            filter: { type: "articles" },
            label: "Article lists",
            Icon: AnnouncementIcon,
            tooltip: "News, opinion and blog series",
        },
        {
            filter: { type: "sdi_group,workarea" },
            label: "Priorities",
            Icon: PriorityHighIcon,
            tooltip: "Strategic Directions and work areas",
        },
        {
            filter: { type: "dossier,dossier_sub" },
            label: "Dossiers",
            Icon: AssignmentIcon,
            tooltip: "Spotlights and campaigns",
        },
        {
            filter: { type: "structure,listing" },
            label: "Structure",
            Icon: ListIcon,
            tooltip: "Structural collections",
        },
        {
            filter: { type: "library" },
            label: "Libraries",
            Icon: LibraryBooksIcon,
            tooltip: "Resolutions, constitution, policy briefs",
        },
        {
            filter: { type: "persons,contacts" },
            label: "Contact lists",
            Icon: ContactPhoneIcon,
            tooltip: "Executive board, staff and unit lists, offices",
        },
        // { filter: { type: "contacts" }, label: "Contacts", Icon: PeopleIcon },
        { filter: { type: "author" }, label: "Authors", Icon: PeopleIcon },
        { filter: { type: "country" }, label: "Countries", Icon: CountryIcon },
        {
            filter: { layout: "region" },
            label: "Regions",
            Icon: LanguageIcon,
        },
        {
            filter: { type: "tag,theme" },
            label: "Tags & Themes",
            Icon: TagIcon,
            tooltip: "Should not be used anymore",
        },
        { filter: {}, label: "All collections" },
    ],
};

const filterToQuery = (filter: { [key: string]: any }) => {
    const result: any = {};
    for (const key in filter) {
        result[`filter[${key}]`] = filter[key];
    }
    return result;
};

const useStyles = makeStyles((theme: Theme) => ({
    listNested: {
        paddingLeft: theme.spacing(4),
    },
}));

export const PrimaryNavigation: React.FC = () => {
    const classes = useStyles();
    const { can, user } = usePage<Page<AuthPageProps & UserPageProps>>().props;
    const [expandCollections, setExpandCollections] = useLocalStorage(
        "ei_expand_collections",
        true
    );
    const [expandItems, setExpandItems] = useLocalStorage(
        "ei_expand_items",
        true
    );
    const [expandUsers, setExpandUsers] = useLocalStorage(
        "ei_expand_users",
        false
    );
    // const { needSave } = useAppContext();
    const needSave = false;

    return (
        <List>
            <ListItem
                button
                onClick={() => Inertia.get(route("admin.dashboard"))}
            >
                <ListItemIcon>
                    <TimelineIcon />
                </ListItemIcon>
                <ListItemText>Dashboard</ListItemText>
            </ListItem>
            <ListItem button onClick={() => setExpandItems(!expandItems)}>
                <ListItemIcon>
                    <FileCopyIcon />
                </ListItemIcon>
                <ListItemText primary="Items" />
                {expandItems ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={expandItems} timeout="auto">
                <List>
                    <ListItem
                        button
                        disabled={needSave}
                        className={classes.listNested}
                        onClick={() => Inertia.get(route("admin.items.create"))}
                    >
                        <ListItemIcon>
                            <AddCircleIcon color="primary" />
                        </ListItemIcon>
                        <ListItemText primary="Create item" />
                    </ListItem>
                    {MENU_ITEMS.items.map(
                        ({ filter, label, Icon, tooltip }, idx) => (
                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(
                                        route(
                                            "admin.items.index",
                                            filterToQuery(filter)
                                        ).toString()
                                    )
                                }
                                className={classes.listNested}
                                key={idx}
                            >
                                <Tooltip title={tooltip || ""}>
                                    <ListItemIcon>
                                        {Icon ? <Icon /> : ""}
                                    </ListItemIcon>
                                </Tooltip>
                                <ListItemText primary={label} />
                            </ListItem>
                        )
                    )}{" "}
                </List>
            </Collapse>
            <ListItem
                button
                onClick={() => setExpandCollections(!expandCollections)}
            >
                <ListItemIcon>
                    <ViewModuleIcon />
                </ListItemIcon>
                <ListItemText primary="Collections" />
                {expandCollections ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={expandCollections} timeout="auto">
                <List>
                    <ListItem
                        button
                        disabled={needSave}
                        className={classes.listNested}
                        onClick={() =>
                            Inertia.get(route("admin.collections.create"))
                        }
                    >
                        <ListItemIcon>
                            <AddCircleIcon color="primary" />
                        </ListItemIcon>
                        <ListItemText primary="Create collection" />
                    </ListItem>
                    {MENU_ITEMS.collections.map(
                        ({ filter, label, Icon, tooltip }, idx) => (
                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(
                                        route(
                                            "admin.collections.index",
                                            filterToQuery(filter)
                                        ).toString()
                                    )
                                }
                                className={classes.listNested}
                                key={idx}
                            >
                                <ListItemIcon>
                                    <Tooltip title={tooltip || ""}>
                                        {Icon ? <Icon /> : <span></span>}
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary={label} />
                            </ListItem>
                        )
                    )}
                </List>
            </Collapse>
            <ListItem button onClick={() => setExpandUsers(!expandUsers)}>
                <ListItemIcon>
                    <AccountBoxIcon />
                </ListItemIcon>
                <ListItemText primary="Account" />
                {expandUsers ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={expandUsers} timeout="auto">
                <List>
                    {can?.users?.create && (
                        <>
                            <ListItem
                                button
                                disabled={needSave || !can?.users?.create}
                                className={classes.listNested}
                                onClick={() =>
                                    Inertia.get(route("admin.users.create"))
                                }
                            >
                                <ListItemIcon>
                                    <AddCircleIcon
                                        color={
                                            can?.users?.create
                                                ? "primary"
                                                : undefined
                                        }
                                    />
                                </ListItemIcon>
                                <ListItemText primary="Add user" />
                            </ListItem>

                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(route("admin.users.index"))
                                }
                                className={classes.listNested}
                            >
                                <ListItemIcon>
                                    <Tooltip title={""}>
                                        <PeopleIcon />
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary="All users" />
                            </ListItem>
                        </>
                    )}
                    {user && (
                        <>
                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(
                                        route("admin.users.edit", { user })
                                    )
                                }
                                className={classes.listNested}
                            >
                                <ListItemIcon>
                                    <Tooltip title={""}>
                                        <ManageAccountsIcon />
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary="My account" />
                            </ListItem>
                            <ListItem
                                button
                                onClick={() => Inertia.post(route("logout"))}
                                className={classes.listNested}
                            >
                                <ListItemIcon>
                                    <Tooltip title={""}>
                                        <LogoutIcon />
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary="Logout" />
                            </ListItem>
                        </>
                    )}
                </List>
            </Collapse>
        </List>
    );
};

export default PrimaryNavigation;
