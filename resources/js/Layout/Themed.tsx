import React from "react";

import {
    createTheme,
    ThemeProvider,
    Theme,
    StyledEngineProvider,
} from "@mui/material/styles";

declare module "@mui/styles/defaultTheme" {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface DefaultTheme extends Theme {}
}

const theme = createTheme({
    components: {
        MuiFormControl: {
            defaultProps: {
                margin: "normal",
            },
        },
        MuiTextField: {
            defaultProps: {
                margin: "normal",
            },
        },
    },
    // palette: { text: { hint: "rgba(0, 0, 0, 0.38)" } },
});

export const Themed: React.FC = ({ children }) => {
    return (
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={theme}>{children}</ThemeProvider>
        </StyledEngineProvider>
    );
};

export default Themed;
