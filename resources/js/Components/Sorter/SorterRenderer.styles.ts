import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme: Theme) => ({
    item: {
        display: "flex",
        width: "100%",
        alignItems: "center",
        borderBottomStyle: "solid",
        borderBottomWidth: 0,
        borderColor: theme.palette.divider,
        paddingBottom: theme.spacing(0.5),
        paddingTop: theme.spacing(0.5),
        "&:first-child": {
            borderTopWidth: 1,
            borderTopStyle: "solid",
        },
        "&:hover": {
            backgroundColor: theme.palette.grey["200"],
        },
    },
    type: {
        flexBasis: "8%",
        flexShrink: 0,
    },
    title: {
        flexBasis: "25%",
        flexShrink: 0,
        flexGrow: 2,
        overflow: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "pre",
    },
    nonResource: {
        // flexBasis: "70%",
    },
    detail: {
        flexBasis: "25%",
        flexShrink: 0,
        flexGrow: 4,
        overflow: "hidden",
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    smallIcon: { fontSize: "1rem" },
}));

export default useStyles;
