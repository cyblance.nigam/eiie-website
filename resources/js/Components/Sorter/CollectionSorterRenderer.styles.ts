import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme: Theme) => ({
    collection: {
        display: "flex",
        whiteSpace: "pre",
        alignItems: "baseline",
        borderRadius: theme.shape.borderRadius,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: theme.palette.divider,
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
        margin: theme.spacing(0.5),
    },
    type: {
        flexBasis: "10%",
        flexShrink: 0,
        marginLeft: theme.spacing(0.5),
    },
    title: { flexGrow: 2 },
    withDrag: {
        paddingLeft: theme.spacing(0),
    },
}));

export default useStyles;
