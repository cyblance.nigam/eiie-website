import React, {
    PropsWithChildren,
    ReactNode,
    useCallback,
    useEffect,
    useState,
} from "react";

import IconButton from "@mui/material/IconButton/IconButton";
import DragIndicatorIcon from "@mui/icons-material/DragIndicator";
import RemoveIcon from "@mui/icons-material/RemoveCircle";
import clsx from "clsx";
import {
    ItemInterface,
    ReactSortable,
    ReactSortableProps,
} from "react-sortablejs";

import useStyles from "./Sorter.styles";

export interface RendererProps<T> {
    item: T;
    dragHandle?: ReactNode;
    removeHandle: ReactNode;
    onChange?: (item: T) => void;
}
type ID = string | number;

interface IProps<T> {
    items: T[];
    className?: string;
    renderer: React.ComponentType<RendererProps<T>>;
    onChange: (items: T[]) => void;
    sortableProps?: ReactSortableProps<ItemInterface>;
    variant?: "column" | "row";
    getId?: (item: T) => ID;
    disableRemove?: boolean;
}
const Sorter = <T extends { id: ID }>({
    items,
    className,
    renderer: Renderer,
    onChange,
    sortableProps,
    variant = "column",
    getId: _getId,
    disableRemove,
}: PropsWithChildren<IProps<T>>) => {
    const classes = useStyles();
    const [list, setList] = useState<ItemInterface[]>([]);

    const getId = useCallback(
        (item: T) => (_getId ? _getId(item) : item.id),
        [_getId]
    );

    useEffect(() => {
        setList(items.map((item) => ({ id: getId(item), item })));
    }, [items, getId]);

    const onEnd = () => {
        onChange(
            list.map(({ item }) => item)
            // [...items].sort(
            //     (a, b) =>
            //         list.findIndex(({ id }) => id === getId(a)) -
            //         list.findIndex(({ id }) => id === getId(b))
            // )
        );
    };

    const onDelete = (item: T) => {
        onChange(items.filter((_item) => getId(_item) !== getId(item)));
    };

    const onChangeItem = (changedItem: T) => {
        onChange(
            items.map((item) =>
                getId(item) === getId(changedItem) ? changedItem : item
            )
        );
    };

    return (
        <ReactSortable
            {...sortableProps}
            list={list}
            setList={setList}
            handle={`.${classes.handle}`}
            className={clsx(
                classes.container,
                {
                    [classes.containerRow]: variant === "row",
                    [classes.containerColumn]: variant === "column",
                },
                className
            )}
            ghostClass={classes.ghost}
            dragClass={classes.drag}
            chosenClass={classes.chosen}
            onEnd={onEnd}
            // disabled={items.length <= 1}
        >
            {list.map(({ item }) => (
                <Renderer
                    item={item}
                    key={getId(item)}
                    onChange={onChangeItem}
                    dragHandle={
                        <DragIndicatorIcon
                            className={clsx(classes.handle, {
                                [classes.disabled]: list.length <= 1,
                            })}
                        />
                    }
                    removeHandle={
                        disableRemove ? undefined : (
                            <IconButton
                                size="small"
                                onClick={() => {
                                    onDelete(item);
                                }}
                                color="secondary"
                            >
                                <RemoveIcon className={classes.smallIcon} />
                            </IconButton>
                        )
                    }
                />
            ))}
        </ReactSortable>
    );
};

export default Sorter;
