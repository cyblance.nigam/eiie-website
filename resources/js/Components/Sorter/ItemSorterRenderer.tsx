import React from "react";

import { Theme, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";

import { Item } from "../../Models";
import { findTitle } from "../../Models/Content";
// import LinkQuery from "../LinkQuery";
import PublishStatusIcon from "../General/PublishStatusIcon";
import { RendererProps } from "../Sorter";

const useStyles = makeStyles((theme: Theme) => ({
    collection: {
        display: "flex",
        maxWidth: "100%",
        whiteSpace: "pre",
        alignItems: "baseline",
        borderRadius: theme.shape.borderRadius,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: theme.palette.divider,
        paddingLeft: theme.spacing(0),
        paddingRight: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    type: {
        flexBasis: "10%",
        flexShrink: 0,
        marginLeft: theme.spacing(0.5),
    },
    title: { flexGrow: 2, textOverflow: "ellipsis", overflow: "hidden" },
}));

export const ItemSorterRenderer: React.FC<RendererProps<Item>> = ({
    item,
    dragHandle,
    removeHandle,
    children,
}) => {
    const classes = useStyles();

    return (
        <div className={classes.collection}>
            {dragHandle}
            <Typography
                variant="body1"
                component="span"
                className={classes.title}
            >
                {(children ? children : findTitle(item)) || "-untitled-"}
            </Typography>
            {<PublishStatusIcon item={item} />}
            <Typography
                variant="caption"
                component="span"
                className={classes.type}
            >
                {item.type}
            </Typography>
            {removeHandle}
        </div>
    );
};

export default ItemSorterRenderer;

// export const CollectionLinkSorterRenderer: React.FC<
//     RendererProps<Collection>
// > = (props) => {
//     const collection = props.item;
//     return (
//         <CollectionSorterRenderer {...props}>
//             <LinkQuery
//                 toRoute="admin.collections.edit"
//                 query={{ id: collection.id }}
//             >
//                 {findTitle(collection) || "-untitled-"}
//             </LinkQuery>
//         </CollectionSorterRenderer>
//     );
// };
