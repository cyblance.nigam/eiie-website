import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";

export default makeStyles((theme: Theme) => ({
    container: { display: "flex" },
    containerColumn: { flexDirection: "column" },
    containerRow: { flexDirection: "row", flexWrap: "wrap" },
    handle: { alignSelf: "center" },
    chosen: { backgroundColor: theme.palette.action.selected },
    ghost: { opacity: 0.5 },
    drag: { backgroundColor: theme.palette.background.paper },
    disabled: { color: theme.palette.action.disabled },
    smallIcon: { fontSize: "1rem" },
}));
