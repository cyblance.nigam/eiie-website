import React, { useState } from "react";

import { Box, TextField, Typography } from "@mui/material";

import { FileResource } from "../../Models";

import { RendererProps } from "./Sorter";
import useStyles from "./SorterRenderer.styles";

export const FileResourceSorterRenderer: React.FC<RendererProps<FileResource>> =
    ({ item: file, dragHandle, removeHandle, onChange }) => {
        const classes = useStyles();
        const [label, setLabel] = useState(file.label || "");

        const onBlur = () => {
            onChange && onChange({ ...file, label });
        };

        return (
            <div className={classes.item}>
                {dragHandle}
                <Box className={classes.title}>
                    <TextField
                        value={label}
                        placeholder="Label"
                        onChange={(evt) => setLabel(evt.target.value)}
                        fullWidth
                        margin="dense"
                        variant="standard"
                        onBlur={onBlur}
                        size="small"
                    />
                </Box>
                <Typography variant="body2" className={classes.detail}>
                    {file.original_filename}
                </Typography>
                {removeHandle}
            </div>
        );
    };

export default FileResourceSorterRenderer;
