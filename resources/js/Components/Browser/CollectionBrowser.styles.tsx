import { makeStyles } from "@mui/styles";
import { Theme } from "@mui/material";

const useStyles = makeStyles((theme: Theme) => ({
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    dialogContainer: {
        padding: theme.spacing(2),
    },
}));

export default useStyles;
