import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    button: {
        marginRight: theme.spacing(2),
    },
}));

export default useStyles;
