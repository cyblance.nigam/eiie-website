import React, { useContext, useEffect, useState } from "react";

import { Box, Button } from "@mui/material";
import {
    GridApiContext,
    GridFooterContainerProps,
    selectedGridRowsCountSelector,
    useGridSelector,
    useGridApiRef,
    useGridRootProps,
    useGridApiContext,
} from "@mui/x-data-grid";

import { useListingContext } from "./ListingContext";
import useStyles from "./ListingSelectFooter.styles";

const ListingSelectFooter: React.FC<GridFooterContainerProps> = (props) => {
    const classes = useStyles();
    const { onCancel, onSelect } = useListingContext();
    const apiRef = useGridApiContext();
    const rootProps = useGridRootProps();
    const selectedRowCount = useGridSelector(
        apiRef,
        selectedGridRowsCountSelector
    );
    const [enable, setEnable] = useState(false);

    const paginationElement = rootProps.pagination &&
        !rootProps.hideFooterPagination &&
        rootProps.components.Pagination && (
            <rootProps.components.Pagination
                {...rootProps.componentsProps?.pagination}
            />
        );

    const onClick = () => {
        const rows = apiRef?.current.getSelectedRows();
        if (rows?.size) {
            onSelect([...rows.values()][0]);
        }
    };

    useEffect(() => {
        setEnable(selectedRowCount > 0);
    }, [selectedRowCount]);

    return (
        <Box display="flex" justifyContent="space-between">
            {paginationElement}
            <Box display="flex" alignItems="center">
                <Button
                    color="primary"
                    variant="text"
                    onClick={() => onCancel && onCancel()}
                    className={classes.button}
                >
                    Cancel
                </Button>
                <Button
                    color="primary"
                    disabled={!enable}
                    onClick={onClick}
                    variant="contained"
                    className={classes.button}
                >
                    Select
                </Button>
            </Box>
        </Box>
    );
};

export default ListingSelectFooter;
