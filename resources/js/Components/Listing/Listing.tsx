import React, { PropsWithChildren } from "react";

import {
    DataGrid,
    GridColDef,
    GridRowModel,
    GridSortModel,
} from "@mui/x-data-grid";

import { IDataSource } from "../useDataSource";

import ListingSelectFooter from "./ListingSelectFooter";

interface IProps<T> {
    columns: GridColDef[];
    dataSource: IDataSource<T>;
    selectMode?: "single" | "multiple";
}
const Listing = <T extends GridRowModel>({
    columns,
    dataSource,
    selectMode = "multiple",
}: PropsWithChildren<IProps<T>>) => {
    const { paginatedData, page, setSort, setPage, loading } = dataSource;

    const _onPageChange = (page: number) => {
        // console.log("onPageChange'", params);
        setPage(page + 1);
    };

    const _onSortModelChange = (model: GridSortModel) => {
        if (model.length) {
            setSort(`${model[0].sort === "desc" ? "-" : ""}${model[0].field}`);
        } else {
            setSort();
        }
    };

    return (
        <DataGrid
            columns={columns}
            rows={paginatedData?.data || []}
            page={page ? page - 1 : 0}
            pageSize={paginatedData?.per_page}
            pagination
            paginationMode="server"
            sortingMode="server"
            rowsPerPageOptions={[paginatedData?.per_page || 16]}
            rowCount={paginatedData?.total}
            checkboxSelection={selectMode === "multiple"}
            disableColumnSelector={true}
            disableColumnMenu={true}
            onPageChange={_onPageChange}
            onSortModelChange={_onSortModelChange}
            hideFooterSelectedRowCount={selectMode === "single"}
            components={
                selectMode === "multiple"
                    ? {}
                    : {
                          Footer: ListingSelectFooter,
                      }
            }
            loading={loading}
        />
    );
};

export default Listing;
