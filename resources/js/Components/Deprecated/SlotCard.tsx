import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import { makeStyles } from "@mui/styles";
import AddIcon from "@mui/icons-material/AddCircleOutline";
import EditIcon from "@mui/icons-material/EditOutlined";
import RemoveIcon from "@mui/icons-material/RemoveCircleOutline";
import cx from "clsx";
import route from "ziggy-js";

import Slot from "../../Models/Slot";

import ButtonConfirmDialog from "../General/ButtonConfirmDialog";
import { Theme } from "@mui/material";

const useStyles = makeStyles((theme: Theme) => ({
    card: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
    },
    content: {
        padding: theme.spacing(1),
        paddingBottom: "0 !important",
        textAlign: "center",
    },
    actions: {
        display: "flex",
        justifyContent: "space-between",
        paddingTop: 0,
        opacity: 0.2,
    },
    hover: {
        opacity: 1,
    },
}));

interface IProps {
    slot: Slot;
    onChange?: (slot: Slot) => void;
}
const SlotCard: React.FC<IProps> = ({ slot: _slot, onChange }) => {
    const classes = useStyles();
    const [slot, setSlot] = useState(_slot);
    const [hover, setHover] = useState(false);

    const onAdd = () => {
        //
    };

    const onDetach = () => {
        setSlot((slot) => ({ ...slot, item_id: undefined }));
    };

    const onEdit = () => {
        if (!slot?.item_id) {
            return;
        }
        Inertia.get(
            route("admin.items.edit", { item: slot.item_id }).toString(),
            {}
        );
    };

    useEffect(() => {
        if (slot !== _slot) {
            onChange && onChange(slot);
        }
    }, [slot, _slot, onChange]);

    useEffect(() => {
        setSlot(_slot);
    }, [_slot]);

    return (
        <Card
            className={classes.card}
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
        >
            <CardContent className={classes.content}>
                <Typography variant="subtitle2">
                    {slot.title?.title ?? "?"}
                </Typography>
                <Typography variant="body2">
                    {slot.item_id ? "Has item" : <>{"No item"}</>}
                </Typography>
            </CardContent>
            <CardActions
                className={cx(classes.actions, hover && classes.hover)}
            >
                <ButtonConfirmDialog
                    buttonProps={{
                        size: "small",
                        disabled: !slot.item_id,
                    }}
                    icon={<RemoveIcon />}
                    onConfirm={onDetach}
                >
                    Detach this item.
                </ButtonConfirmDialog>
                <ButtonConfirmDialog
                    buttonProps={{
                        size: "small",
                        disabled: !slot.item_id,
                    }}
                    icon={<EditIcon />}
                    onConfirm={onEdit}
                >
                    Leave this screen to edit the slotted item, unsaved changes
                    will be lost.
                </ButtonConfirmDialog>
                <IconButton
                    color="secondary"
                    onClick={onAdd}
                    size="small"
                    disabled={!!slot.item_id}
                >
                    <AddIcon />
                </IconButton>
            </CardActions>
        </Card>
    );
};

export default SlotCard;
