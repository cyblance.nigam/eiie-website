import { jsx } from "slate-hyperscript";
import { deserialize, getGenericAttributes } from "../../deserialize";
import { CanDeserializeFunction, DeserializeFunction } from "../types";

export const canDeserializeItem: CanDeserializeFunction = (el) => {
    return false;
};
const deserializeItem: DeserializeFunction = (el) => {};

export default deserializeItem;
