import { makeStyles } from "@mui/styles";
import { Theme } from "@mui/material";
const useStyles = makeStyles((theme: Theme) => ({
    iframe: {
        maxWidth: 200,
        maxHeight: 200,
    },
}));

export default useStyles;
