import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import {
    Box,
    Button,
    Link,
    TextField,
    Tooltip,
    Typography,
} from "@mui/material";
import OpenInNewIcon from "@mui/icons-material/OpenInNew";
import route from "ziggy-js";

import ButtonConfirmDialog from "../../../General/ButtonConfirmDialog";
import { IPaneProps } from "../../Toolbar/ButtonDialog";

import useStyles from "./EditPane.styles";
import { LinkAttributes, LinkElementType } from "./type";

// interface LinkAttributes extends Attributes {
//     url?: string;
// }

type IProps = IPaneProps<LinkElementType>;
const LinkEditPane: React.FC<IProps> = ({ /*item,*/ elementIn, onChange }) => {
    const [attributes, setAttributes] = useState<LinkAttributes>({});
    const classes = useStyles();
    const [allowEditUrl, setAllowEditUrl] = useState(false);
    const [linkType, setLinkType] = useState("");

    // useEffect(() => {
    //     if (!item) {
    //         console.log("no item", item);
    //         return;
    //     }
    //     const content = findContent(item, language || "*");
    //     if (!content) {
    //         setError("No Link content found, this is an error");
    //         setLinkResource(undefined);
    //         return;
    //     }
    //     const link = content.links[0];
    //     setLinkResource(link);
    //     if (!link || !link.url) {
    //         setError("No Link details found, this is an error");
    //         return;
    //     }
    //     setError(undefined);
    //     setTitle(content.title);
    //     setAttributes((attr) => ({
    //         ...attr,
    //         dataId: String(item.id),
    //         url: link.url,
    //     }));
    // }, [item, elementIn, language]);

    // useEffect(() => {
    //     console.log("elementIncoming", elementIn, item);
    //     if (elementIn) {
    //         setAttributes((attr) => ({ ...attr, url: elementIn.url }));
    //     } else if (item) {
    //         const content = findContent(item, language || "*");
    //         if (!content) {
    //             setError("No link resource content found, this is an error");
    //             // setLinkResource(undefined);
    //             return;
    //         }
    //         const link = content.links[0];
    //         // setLinkResource(link);
    //         if (!link || !link.url) {
    //             setError("No Link details found, this is an error");
    //             return;
    //         }
    //         setError(undefined);
    //         setTitle(content.title);
    //         setAttributes((attr) => ({
    //             ...attr,
    //             dataId: String(item.id),
    //             url: link.url,
    //         }));
    //     }
    // }, [elementIn, item, language]);

    useEffect(() => {
        console.log("elementIncoming", elementIn);
        if (elementIn) {
            setAttributes((attrs) => ({
                ...attrs,
                dataId: elementIn.dataId,
                dataType: elementIn.dataType,
                url: elementIn.url,
                linkType: elementIn.linkType,
            }));
        }
    }, [elementIn]);

    useEffect(() => {
        setAllowEditUrl(attributes.linkType === "external");
        setLinkType(
            attributes.linkType === "external"
                ? "Link (without resource)"
                : attributes.linkType === "resource"
                ? "Link resource"
                : "Internal link"
        );
    }, [attributes]);

    useEffect(() => {
        onChange({
            ...attributes,
            type: "link",
            children: elementIn?.children || [{ text: "" }],
        });
    }, [attributes, onChange, elementIn]);

    return (
        <>
            <Box>
                <Box
                    display="flex"
                    flexDirection="column"
                    className={classes.linkPreview}
                >
                    {elementIn ? (
                        <>
                            <Typography variant="subtitle1">
                                {linkType}
                            </Typography>
                            {allowEditUrl ? (
                                <TextField
                                    fullWidth
                                    variant="outlined"
                                    label="URL"
                                    value={attributes.url || ""}
                                    placeholder="http:// or https:// required"
                                    onChange={(e) =>
                                        setAttributes((attr) => ({
                                            ...attr,
                                            url: e.target.value,
                                        }))
                                    }
                                    InputLabelProps={{ shrink: true }}
                                />
                            ) : attributes.linkType === "resource" ? (
                                <Box
                                    display="flex"
                                    justifyContent="space-between"
                                >
                                    <Tooltip title={attributes.url || false}>
                                        <div>
                                            <Button
                                                variant="text"
                                                endIcon={<OpenInNewIcon />}
                                                disabled={!attributes.url}
                                                fullWidth={false}
                                                component={Link}
                                                target="_blank"
                                                href={attributes.url}
                                                rel="noopener noreferrer"
                                            >
                                                Preview
                                            </Button>
                                        </div>
                                    </Tooltip>
                                    {attributes.dataId && false && (
                                        <ButtonConfirmDialog
                                            label="Edit Link Resource"
                                            onConfirm={() => {
                                                Inertia.get(
                                                    route("admin.items.edit", {
                                                        id: attributes.dataId!,
                                                    }).toString(),
                                                    {}
                                                );
                                            }}
                                        />
                                    )}
                                </Box>
                            ) : (
                                <></>
                            )}
                        </>
                    ) : (
                        <Typography variant="subtitle1">
                            Invalid link
                        </Typography>
                    )}
                </Box>
            </Box>
        </>
    );
};

export default LinkEditPane;
