import { WithEditorFunction } from "../types";

const withImage: WithEditorFunction = (editor) => {
    return editor;
};

export default withImage;
