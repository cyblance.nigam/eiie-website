import { makeStyles } from "@mui/styles";
const useStyles = makeStyles(() => ({
    image: {
        maxWidth: 200,
        maxHeight: 200,
    },
}));

export default useStyles;
