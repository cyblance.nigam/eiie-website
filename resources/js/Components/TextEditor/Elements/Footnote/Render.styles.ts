import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme: Theme) => ({
    footnoteOrder: {
        float: "left",
        paddingRight: theme.spacing(1),
    },
}));

export default useStyles;
