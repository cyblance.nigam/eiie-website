import React from "react";

import clsx from "clsx";
import { RenderElementProps, useSelected, useSlate } from "slate-react";

import useStyles from "./Render.styles";
import { typeFootnote, typeFootnoteAnchor } from "./type";

export const FootnoteRender: React.FC<RenderElementProps> = ({
    attributes,
    element,
    children,
}) => {
    const selected = useSelected();
    const editor = useSlate();
    const classes = useStyles();

    if (element.type !== typeFootnote) {
        return <></>;
    }
    const { uuid } = element;
    const order = editor.footnoteOrder(uuid);

    return (
        <div
            {...attributes}
            className={clsx({ selected })}
            data-footnote-order={`[${order > 0 ? order : "_"}]`}
        >
            <span contentEditable={false} className={classes.footnoteOrder}>
                [{order > 0 ? order : "_"}]
            </span>
            {children}
        </div>
    );
};

export default FootnoteRender;

export const FootnoteAnchorRender: React.FC<RenderElementProps> = ({
    attributes,
    element,
    children,
}) => {
    const selected = useSelected();
    const editor = useSlate();

    if (element.type !== typeFootnoteAnchor) {
        return <></>;
    }
    const { ref } = element;
    const order = editor.footnoteOrder(ref);

    return (
        <a
            {...attributes}
            href={`#footnote_${ref}`}
            className={clsx("footnote-anchor", { selected })}
        >
            [{order}]{children}
        </a>
    );
};
