import { WithEditorFunction } from "../types";

const withList: WithEditorFunction = (editor) => {
    return editor;
};

export default withList;
