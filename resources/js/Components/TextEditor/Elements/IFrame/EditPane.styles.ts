import { makeStyles } from "@mui/styles";
const useStyles = makeStyles(() => ({
    linkPreview: {
        width: 400,
    },
}));

export default useStyles;
