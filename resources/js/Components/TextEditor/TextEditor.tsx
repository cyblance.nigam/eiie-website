import React, { useEffect, useMemo, useState } from "react";

import { Box, useFormControl } from "@mui/material";
import clsx from "clsx";
import { isEqual } from "lodash";
import { useDebounce, usePrevious } from "react-use";
import {
    Editor,
    createEditor,
    Descendant,
    Transforms,
    Node,
    Element,
    Text,
} from "slate";
import { withHistory } from "slate-history";
import { Editable, Slate, withReact } from "slate-react";

import { TEXTEDIT_DEBOUNCE_MS } from "../../Config";

import { parse } from "./deserialize";
import { withElements } from "./Elements";
import { RenderElement, RenderLeaf } from "./render";
// import format from "./serialize";
import useStyles from "./TextEditor.styles";
import Toolbar from "./Toolbar/Toolbar";
import withHtml from "./withHtml";

interface IProps {
    text?: string;
    json?: string;
    // onChange: (text: string) => void;
    onChangeJson: (json: string | null) => void;
    mode: "full" | "limited";
}
const TextEditor: React.FC<IProps> = ({
    text: _text,
    json: _json,
    mode,
    // onChange,
    onChangeJson,
}) => {
    const [text] = useState(_text);
    const [json] = useState(_json);
    const [originalJsonObj] = useState(JSON.parse(_json || "[]"));
    const classes = useStyles();
    const editor = useMemo(
        () => withElements(withHtml(withHistory(withReact(createEditor())))),
        []
    );
    const [jsonValue, setJsonValue] = useState<any>();
    const previousJsonValue = usePrevious(jsonValue);

    const [value, setValue] = useState<Descendant[]>([]);
    const [fullscreen, setFullscreen] = useState(false);
    // const [formattedText, setFormattedText] = useState<string>();
    const previousTextIn = usePrevious(text);
    const ctx = useFormControl();

    useEffect(() => {
        if (text && !json) {
            if (text !== previousTextIn) {
                const parsed = parse(text || "");
                setJsonValue((jsonVal: any) =>
                    isEqual(jsonVal, parsed) ? jsonVal : parsed
                );
                // onChangeJson(JSON.stringify(parsed));
            }
        } else if (json) {
            const parsed = JSON.parse(json);
            if (!parsed?.length) {
                setJsonValue([{ type: "paragraph", children: [{ text: "" }] }]);
            } else {
                setJsonValue((jsonVal: any) =>
                    isEqual(jsonVal, parsed) ? jsonVal : parsed
                );
            }
        } else {
            setJsonValue([{ type: "paragraph", children: [{ text: "" }] }]);
        }
    }, [text, previousTextIn, json]);

    useEffect(() => {
        if (!jsonValue) {
            return;
        }
        if (isEqual(jsonValue, previousJsonValue)) {
            return;
        }
        editor.children = jsonValue.filter((node: any) => {
            if (node.type === "paragraph") {
                if (
                    node.children.find(
                        (n: Node) =>
                            Element.isElement(n) ||
                            (Text.isText(n) && n.text.trim())
                    )
                ) {
                    return true;
                } else {
                    return false;
                }
            }
            return true;
        });
        if (!editor.children.length) {
            // minimum 1 paragraph
            editor.children = [{ type: "paragraph", children: [{ text: "" }] }];
        }
        if (editor.selection) {
            Transforms.deselect(editor);
        }
        Editor.normalize(editor, { force: true });
        setValue(editor.children);
    }, [jsonValue, previousJsonValue, editor]);

    // useEffect(() => {
    //     if (previousTextIn === text) {
    //         // console.log("no change in text");
    //         return;
    //     }
    //     if (text === formattedText) {
    //         // console.log("we got our formatted text back, don't parse it again");
    //         return;
    //     }
    //     // console.log("going to parse", text, formattedText);
    //     const parsed = parse(text || "");
    //     // https://github.com/ianstormtaylor/slate/issues/3465
    //     // const selection = editor.selection;
    //     if (editor.selection) {
    //         Transforms.deselect(editor);
    //         // console.log(
    //         //     "Editor selection Range start",
    //         //     Range.start(editor.selection)
    //         // );
    //         // if (editor.selection.anchor.path[0] >= parsed.length) {
    //         //     console.log("reset selection to [0,0]");
    //         //     Transforms.select(editor, [0, 0]);
    //         // } else if (parsed[editor.selection.anchor.path[0]]) {
    //         //     // console.log(
    //         //     //     "parsed availabele at path[0]",
    //         //     //     parsed[editor.selection.anchor.path[0]]
    //         //     // );
    //         // }
    //     }
    //     editor.children = parsed;
    //     Editor.normalize(editor, { force: true });
    //     setValue(editor.children);
    // }, [text, previousTextIn, formattedText, editor]);

    /*
    const [, cancelDebouncer] = useDebounce(
        () => {
            const formatted = format(value);
            setFormattedText(formatted);
            // if (formatted !== "<p></p>") {
            onChange(formatted);
            // }
        },
        1500,
        [value]
    );
        */
    const [, cancelDebouncer] = useDebounce(
        () => {
            if (!isEqual(value, originalJsonObj)) {
                if (
                    value.length === 1 &&
                    Element.isElement(value[0]) &&
                    (value[0] as Element).children.length === 1 &&
                    Text.isText((value[0] as Element).children[0]) &&
                    ((value[0] as Element).children[0] as Text).text === ""
                ) {
                    // emit null
                    onChangeJson(null);
                } else {
                    const json = JSON.stringify(value);
                    onChangeJson(json);
                }
            }
        },
        TEXTEDIT_DEBOUNCE_MS,
        [value]
    );

    useEffect(() => {
        return cancelDebouncer;
    }, [cancelDebouncer]);

    return (
        <Box
            className={clsx(classes.root, fullscreen ? classes.fullscreen : "")}
        >
            <Box
                className={clsx(classes.border, {
                    [classes.focused]: ctx?.focused,
                })}
            ></Box>
            <Slate editor={editor} value={value} onChange={setValue}>
                <Toolbar
                    mode={mode}
                    fullscreen={fullscreen}
                    setFullscreen={setFullscreen}
                    className={classes.toolbar}
                />
                <Editable
                    onFocus={ctx?.onFocus}
                    onBlur={ctx?.onBlur}
                    renderElement={RenderElement}
                    renderLeaf={RenderLeaf}
                    className={classes.editarea}
                />
            </Slate>
        </Box>
    );
};

export default TextEditor;
