import React from "react";

import { Box } from "@mui/material";
import clsx from "clsx";
import { useSlate } from "slate-react";

import { BlockElementTypes } from "./BlockButton";
import { isBlockActiveMulti } from "./BlockButton.functions";
import useStyles from "./Toolbar.styles";

interface IProps {
    baseButton: React.ReactNode;
    formats: BlockElementTypes[];
}
const MultiButton: React.FC<IProps> = ({ formats, baseButton, children }) => {
    const editor = useSlate();
    // const [isOpen, setIsOpen] = useState(false);
    // const anchorEl = useRef<HTMLSpanElement>(null);
    const classes = useStyles();

    // useEffect(() => {
    //     setIsOpen(isBlockActiveMulti(editor, formats));
    // }, [editor, editor.selection, formats]);

    const showOptions = isBlockActiveMulti(editor, formats);

    return (
        <>
            <Box
                display="flex"
                flexWrap="nowrap"
                className={clsx({ [classes.showMultiButtonPane]: showOptions })}
            >
                {baseButton}
                <Box className={clsx(classes.multiButtonPane)}>{children}</Box>
            </Box>
            {/* <Popper
                placement="top-start"
                open={isOpen}
                anchorEl={anchorEl.current}
                disablePortal={true}
            >
                <div className={classes.multiButtonPane}>{children}</div>
            </Popper> */}
        </>
    );
};

export default MultiButton;
