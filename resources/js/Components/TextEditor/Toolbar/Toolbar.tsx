import React from "react";

import { Box, IconButton } from "@mui/material";
import FormatSizeIcon from "@mui/icons-material/FormatSize";
import FullscreenIcon from "@mui/icons-material/Fullscreen";
import FullscreenExitIcon from "@mui/icons-material/FullscreenExit";
import Looks3Icon from "@mui/icons-material/Looks3";
import Looks4Icon from "@mui/icons-material/Looks4";
import Looks5Icon from "@mui/icons-material/Looks5";

// import Looks6Icon from "@mui/icons-material/Looks6";
import {
    ImageElement,
    LinkElement,
    QuoteElement,
    VideoElement,
    OEmbedElement,
    OrderedListElement,
    UnorderedListElement,
    FootnoteElement,
} from "../Elements";
import IFrameElement from "../Elements/IFrame";

import BlockButton from "./BlockButton";
import HistoryButton from "./HistoryButton";
import { BoldButton, ItalicButton } from "./MarkButtons";
import MultiButton from "./MultiButton";

interface IProps {
    mode?: "full" | "limited";
    fullscreen?: boolean;
    setFullscreen?: (fullscreen: boolean) => void;
    className?: string;
}
const Toolbar: React.FC<IProps> = ({
    mode = "full",
    fullscreen,
    setFullscreen,
    className,
}) => {
    // const editor = useSlate();

    return (
        <Box
            display="flex"
            justifyContent="space-between"
            className={className}
            // onClick={(evt) => {
            //     // evt.stopPropagation();
            //     // evt.preventDefault();
            // }}
        >
            <Box display="flex">
                <HistoryButton undo />
                <HistoryButton redo />

                <BoldButton />
                <ItalicButton />

                <OrderedListElement.ToolbarButton />
                <UnorderedListElement.ToolbarButton />

                <MultiButton
                    formats={["heading"]}
                    baseButton={
                        <BlockButton format="heading" attributes={{ level: 3 }}>
                            <FormatSizeIcon />
                        </BlockButton>
                    }
                >
                    <BlockButton format="heading" attributes={{ level: 3 }}>
                        <Looks3Icon />
                    </BlockButton>
                    <BlockButton format="heading" attributes={{ level: 4 }}>
                        <Looks4Icon />
                    </BlockButton>
                    <BlockButton format="heading" attributes={{ level: 5 }}>
                        <Looks5Icon />
                    </BlockButton>
                    {/* <BlockButton format="heading-6">
                    <Looks6Icon />
                </BlockButton> */}
                </MultiButton>

                <LinkElement.ToolbarButton />
                <QuoteElement.ToolbarButton />
                {mode === "full" && (
                    <>
                        <ImageElement.ToolbarButton />
                        <VideoElement.ToolbarButton />
                        <OEmbedElement.ToolbarButton />
                        <IFrameElement.ToolbarButton />
                        <FootnoteElement.ToolbarButton />
                    </>
                )}
            </Box>
            <Box display="flex">
                {setFullscreen && (
                    <IconButton
                        onClick={() => setFullscreen(!fullscreen)}
                        size="small"
                    >
                        {fullscreen ? (
                            <FullscreenExitIcon />
                        ) : (
                            <FullscreenIcon />
                        )}
                    </IconButton>
                )}
            </Box>
        </Box>
    );
};

export default Toolbar;
