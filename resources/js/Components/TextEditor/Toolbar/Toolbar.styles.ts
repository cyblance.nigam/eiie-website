import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    button: {
        borderRadius: "0.1em",
    },
    buttonActive: {
        color: theme.palette.primary.main,
        backgroundColor: theme.palette.grey[200],
    },
    multiButtonPane: {
        backgroundColor: theme.palette.background.paper,
        boxSizing: "border-box",
        position: "relative",
        maxWidth: 0,
        overflow: "hidden",
        display: "flex",
        alignItems: "center",
        flexWrap: "nowrap",
        transition: "all 0.2s linear",
    },
    showMultiButtonPane: {
        "& $multiButtonPane": {
            maxWidth: 150,
            borderRight: `1px solid ${theme.palette.divider}`,
        },
    },
}));

export default useStyles;
