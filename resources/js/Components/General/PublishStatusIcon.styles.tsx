import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    icon: {
        fontSize: theme.typography.fontSize,
        color: theme.palette.grey["800"],
        marginLeft: theme.spacing(0.5),
    },
}));

export default useStyles;
