import { makeStyles } from "@mui/styles";
const useStyles = makeStyles(() => ({
    input: {
        "& input": { opacity: 1.0 },
    },
}));

export default useStyles;
