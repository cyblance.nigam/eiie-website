import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import {
    Card,
    CardActions,
    CardContent,
    CardMedia,
    Table,
    TableBody,
    TableCell,
    TableRow,
    Theme,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import RemoveIcon from "@mui/icons-material/RemoveCircle";
import { makeStyles } from "@mui/styles";
import cx from "clsx";
import route from "ziggy-js";

import { ItemSubtype } from "../../Config";
import { useAppContext } from "../../Layout";
import { findContent, ImageResource, Item } from "../../Models";
import { useLanguage } from "../../Utils/LanguageContext";
import useImageUrl from "../../Utils/useImageUrl";

import ButtonConfirmDialog from "./ButtonConfirmDialog";

const useStyles = makeStyles((theme: Theme) => ({
    image: {
        backgroundSize: "contain",
        backgroundColor: theme.palette.background.default,
        paddingTop: `${900 / 16}%`,
        margin: "0 auto",
    },
    square: { maxWidth: `${900 / 16}%` },
    landscape: {},
    portrait: { maxWidth: `${900 / 16 / 2}%` },
    actions: {
        display: "flex",
        justifyContent: "space-between",
    },
    cell: {
        padding: "1px 3px 1px 0px !important",
        verticalAlign: "top",
        lineHeight: 1.4,
    },
}));

interface BaseProps {
    // image?: Item;
    // imageResource?: ImageResource;
    // imageType?: ItemSubtype;
    imageOnly?: boolean;
    support_color?: string;
    onRemoved?: () => void;
    className?: string;
}
interface ImageProps extends BaseProps {
    image?: Item;
    imageResource?: ImageResource;
    imageType?: ItemSubtype;
}
interface ImageResourceProps extends BaseProps {
    image?: never;
    imageResource: ImageResource;
    imageType: ItemSubtype;
}
const ImageCard: React.FC<ImageResourceProps | ImageProps> = ({
    image,
    imageResource,
    imageType,
    imageOnly,
    className,
    support_color,
    onRemoved,
}) => {
    const classes = useStyles();
    const [resource, setResource] = useState<ImageResource | undefined>(
        imageResource
    );
    const [caption, setCaption] = useState("");
    const imgPath = useImageUrl(resource, { w: 300 });
    const { needSave } = useAppContext();
    const lang = useLanguage();

    useEffect(() => {
        setResource(imageResource);
    }, [imageResource]);

    useEffect(() => {
        if (!image) {
            return;
        }
        const content =
            findContent(image, lang) || image.contents[0] || image.content;
        if (content?.images?.length && content.images[0].path) {
            setResource(content.images[0]);
            setCaption(content.subtitle || "");
        }
    }, [image, lang]);

    if (!resource || !imgPath) {
        return (
            <Card>
                <CardContent>No image resource found!</CardContent>
            </Card>
        );
    }

    const onEdit = () => {
        Inertia.get(
            route("admin.items.edit", { item: image!.id }).toString(),
            {}
        );
    };

    const imgRatioClass =
        (image &&
            (image.subtype === "image.square" ||
                image.subtype === "image.icon")) ||
        (imageType &&
            (imageType === "image.square" || imageType === "image.icon"))
            ? classes.square
            : classes.landscape;

    return (
        <Card className={className}>
            <CardMedia
                image={imgPath}
                className={cx(classes.image, imgRatioClass)}
                style={{ backgroundColor: support_color }}
            />
            {!imageOnly && (
                <>
                    <CardContent>
                        <Table size="small">
                            <TableBody>
                                <TableRow>
                                    <TableCell
                                        className={classes.cell}
                                        variant="head"
                                    >
                                        type:
                                    </TableCell>
                                    <TableCell className={classes.cell}>
                                        {(image ? image.subtype : imageType) ??
                                            "normal"}
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell
                                        className={classes.cell}
                                        variant="head"
                                    >
                                        caption:
                                    </TableCell>
                                    <TableCell className={classes.cell}>
                                        {caption ?? "-not set-"}
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell
                                        className={classes.cell}
                                        variant="head"
                                    >
                                        languages:
                                    </TableCell>
                                    <TableCell className={classes.cell}>
                                        {image?.contents
                                            ?.map((c) => c.lang)
                                            .join(", ")}
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </CardContent>
                    <CardActions className={classes.actions} disableSpacing>
                        <ButtonConfirmDialog
                            label="Remove"
                            buttonProps={{ size: "small" }}
                            icon={<RemoveIcon />}
                            onConfirm={() => {
                                onRemoved && onRemoved();
                            }}
                        >
                            Remove this image from this context, note that it
                            will not be deleted so it can still be used in other
                            contexts.
                        </ButtonConfirmDialog>
                        <ButtonConfirmDialog
                            label="Edit"
                            buttonProps={{ size: "small" }}
                            icon={<EditIcon />}
                            onConfirm={onEdit}
                            needConfirmation={needSave}
                        >
                            Leave this screen to edit the image, unsaved changes
                            will be lost.
                        </ButtonConfirmDialog>
                    </CardActions>
                </>
            )}
        </Card>
    );
};

export default ImageCard;
