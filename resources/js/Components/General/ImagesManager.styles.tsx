import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
    },
    addContainer: {
        alignSelf: "center",
        justifyContent: "center",
    },
    addButton: { flexDirection: "column" },
    hugeIcon: { fontSize: "3rem" },
}));

export default useStyles;
