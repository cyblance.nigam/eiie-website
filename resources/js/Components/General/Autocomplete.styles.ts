import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    collection: {
        display: "flex",
        width: "100%",
        whiteSpace: "pre",
        alignItems: "baseline",
        borderRadius: theme.shape.borderRadius,
        borderStyle: "solid",
        borderWidth: 0,
        borderColor: theme.palette.divider,
        paddingLeft: theme.spacing(0),
        // paddingRight: theme.spacing(1),
        // marginRight: theme.spacing(1),
        // marginBottom: theme.spacing(1),
    },
    title: { flexGrow: 0 },
    type: {
        // flexBasis: "10%",
        flexShrink: 0,
        marginLeft: theme.spacing(0.5),
    },
    itemsCount: {
        flexGrow: 1,
        textAlign: "end",
        flexShrink: 0,
        alignSelf: "flex-end",
    },

    importButton: {
        marginLeft: theme.spacing(2),
    },
}));

export default useStyles;
