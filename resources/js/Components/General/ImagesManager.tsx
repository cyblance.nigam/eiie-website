import React from "react";

import { Typography } from "@mui/material";
import Grid from "@mui/material/Grid";

import Item from "../../Models/Item";
import usePatcher from "../../Stores/usePatcher";
import { ImageMenu, ResourcePicker } from "../Browser";
import Section, { ImagesSummary } from "../Section";

import ColorPickerInput from "./ColorPickerInput";
import ImageCard from "./ImageCard";
import useStyles from "./ImagesManager.styles";

interface IProps {
    images: Item[];
    support_color?: string;
    // onChange: (images: Item[]) => void;
}
const ImagesManager: React.FC<IProps> = ({ images, support_color }) => {
    const classes = useStyles();
    // const [images, setImages] = useState(_images);
    const patch = usePatcher();

    const onRemoved = (image: Item) => {
        // setImages((images) => images.filter((img) => img.id !== image.id));
        patch(
            "all_images",
            images.filter((img) => img.id !== image.id)
        );
    };

    const onAdd = (item: Item) => {
        // setImages((images) => [...images, item]);
        patch("all_images", [...images, item]);
    };

    const color = support_color ? `#${support_color}` : undefined;
    const onChangeColor = (hex?: string) => {
        patch("support_color", hex?.replace("#", "") || "");
    };
    // useEffect(() => {
    //     if (images !== _images) {
    //         onChange(images);
    //     }
    // }, [images, _images, onChange]);

    // useEffect(() => {
    //     setImages(_images);
    // }, [_images]);

    return (
        <Section
            title={`Images and support color`}
            summary={
                <ImagesSummary
                    images={images}
                    support_color={color}
                    addImageButton={
                        <ResourcePicker
                            label="Add image"
                            onPick={onAdd}
                            menu={ImageMenu}
                        />
                    }
                />
            }
        >
            <Grid container spacing={2}>
                {images.map((img, i) => (
                    <Grid item xs={3} key={i}>
                        <ImageCard
                            image={img}
                            onRemoved={() => onRemoved(img)}
                            support_color={color}
                        />
                        {/* <ImageLanguagesPicker contents={img.contents} /> */}
                    </Grid>
                ))}
                <Grid
                    item
                    xs={3}
                    container
                    spacing={0}
                    className={classes.addContainer}
                >
                    <ResourcePicker
                        label="Add image"
                        onPick={onAdd}
                        menu={ImageMenu}
                    />
                    {/* <Button onClick={onAdd} color="secondary">
                    <Box
                        display="flex"
                        flexDirection="column"
                        alignItems="center"
                    >
                        <AddIcon className={classes.hugeIcon} />
                        Add image
                    </Box>
                </Button> */}
                </Grid>
                <Grid item xs={12}>
                    <ColorPickerInput
                        label="Support color"
                        color={color}
                        onChange={(hex) => onChangeColor(hex)}
                    />
                    <br />
                    <Typography variant="caption">
                        The support color is used in certain situations to
                        provide extra visual differentation.
                    </Typography>
                </Grid>
            </Grid>
        </Section>
    );
};

export default ImagesManager;
