import React, { ReactNode } from "react";

import {
    AutocompleteRenderOptionState,
    Box,
    Typography,
    AutocompleteRenderGetTagProps,
} from "@mui/material";
import { ClassNameMap } from "@mui/styles";

import { Collection, findTitle, isItem, Item } from "../../Models";
import Sorter from "../Sorter";
import ItemSorterRenderer from "../Sorter/ItemSorterRenderer";

import {
    FGetOptionLabel,
    FGroupByOption,
    NamedOption,
    TitledOption,
} from "./Autocomplete.types";

export const getContentTitleOptionLabel = (val: Item | Collection) => {
    if (isItem(val)) {
        return `${findTitle(val) || "-untitled-"} ${
            val.affiliate?.acronym ? `(${val.affiliate?.acronym})` : ""
        }`;
    } else {
        return `${findTitle(val) || "-untitled-"}`;
    }
};

export const getNamedOptionLabel: FGetOptionLabel<NamedOption> = ({ name }) =>
    name;
export const getTitledOptionLabel: FGetOptionLabel<TitledOption> = ({
    title,
}) => title;

export const renderContentOption = (
    props: React.HTMLAttributes<HTMLLIElement>,
    option: Item | Collection,
    _state: AutocompleteRenderOptionState,
    classes: ClassNameMap
) => {
    return (
        <li {...props} key={option.id}>
            <Box className={classes.collection}>
                <Typography
                    variant="body1"
                    component="span"
                    className={classes.title}
                >
                    {findTitle(option) || "-untitled-"}
                </Typography>
                <Typography
                    variant="caption"
                    component="span"
                    className={classes.type}
                >
                    {option.type}
                </Typography>
                {!isItem(option) ? (
                    <Typography
                        variant="body1"
                        component="span"
                        className={classes.itemsCount}
                    >
                        #{option.items_count}
                    </Typography>
                ) : undefined}
            </Box>
        </li>
    );
};

export const renderTagsSortable = (
    values: Item[],
    getTagProps: AutocompleteRenderGetTagProps,
    onReorder: (values: Item[]) => void
): ReactNode => {
    return (
        <div>
            <Sorter
                items={values}
                renderer={ItemSorterRenderer}
                onChange={onReorder}
                variant="row"
            />
        </div>
    );
};

export const groupByParentCollection: FGroupByOption<Collection> = (
    collection
) => {
    if (collection.sub_collections?.length) {
        // I have subcollections, use own title as group
        return findTitle(collection) || "?";
    }
    if (collection.parent_collections?.length) {
        return findTitle(collection.parent_collections[0]) || "??";
    }
    return "???";
};
