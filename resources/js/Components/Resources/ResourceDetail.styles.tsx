import { makeStyles } from "@mui/styles";
const useStyles = makeStyles(() => ({
    container: {
        width: "100%",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
    },
    item: {
        overflow: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "pre",
        textAlign: "left",
    },
    preview: {},
    previewIcon: {
        fontSize: "1rem",
    },
}));

export default useStyles;
