import React, { useCallback, useEffect, useState } from "react";

import {
    FormControl,
    Grid,
    InputLabel,
    TextField,
    Typography,
} from "@mui/material";
import { pascalCase } from "pascal-case";
import route from "ziggy-js";

import { DCProject, Item, ItemContent } from "../../Models";
import { EditorProps } from "../ContentsEditor";
import Autocomplete from "../General/Autocomplete";
import { getContentTitleOptionLabel } from "../General/Autocomplete.renderers";
import DatePicker from "../General/DatePicker";
import TextEditor from "../TextEditor";

import {
    DCPROJECT_FIELDS,
    DCPROJECT_HTMLFIELDS,
} from "./EditorDCProject.constants";
import useStyles from "./EditorDCProject.styles";

const EditorDCProject: React.FC<EditorProps<ItemContent>> = ({
    content,
    onChange,
}) => {
    const classes = useStyles();
    const [dcProject, setDcProject] = useState<DCProject | undefined>(
        content?.dcprojects ? content?.dcprojects[0] : undefined
    );

    useEffect(() => {
        setDcProject(content?.dcprojects[0]);
    }, [content.dcprojects]);

    const setField = useCallback((name: string, value?: string | Item[]) => {
        setDcProject((dcProject) => {
            if (!dcProject) {
                return { [name]: value } as unknown as DCProject;
            }
            if ((dcProject as any)[name] === value) {
                return dcProject;
            } else {
                return { ...dcProject, [name]: value };
            }
        });
    }, []);

    const onBlur = () => {
        if (dcProject !== content.dcprojects[0]) {
            onChange("dcprojects", [dcProject]);
        }
    };

    if (!dcProject) {
        return (
            <Typography color="error">
                No development cooperation project info found, this is an error.
            </Typography>
        );
    }

    return (
        <Grid container spacing={1}>
            <Grid item xs={12}>
                <Typography variant="h6">
                    Development cooperation project
                </Typography>
            </Grid>

            <Grid item xs={12}>
                <TextField
                    value={dcProject.host_orgs_str ?? ""}
                    label="Host organisations"
                    variant="outlined"
                    fullWidth
                    onChange={(e) =>
                        setField("host_orgs_str", e.target.value as string)
                    }
                    onBlur={onBlur}
                    margin="dense"
                    InputLabelProps={{ shrink: true }}
                    helperText={
                        dcProject.host_orgs_str
                            ? "When this field is empty the members below will be used"
                            : undefined
                    }
                />
            </Grid>
            <Grid item xs={12}>
                <Autocomplete<Item>
                    values={dcProject.host_organisations}
                    label="NOT WORKING YET - Host organisations (link to members)"
                    onChange={(orgs) => setField("host_organisations", orgs)}
                    dataSource={{
                        filter: { filter: { type: "affiliate" } },
                        xhrUrl: route("admin.items.index"),
                    }}
                    getOptionLabel={getContentTitleOptionLabel}
                />
            </Grid>

            <Grid item xs={12}>
                <TextField
                    value={dcProject.coop_orgs_str ?? ""}
                    label="Cooperation organisations"
                    variant="outlined"
                    fullWidth
                    onChange={(e) =>
                        setField("coop_orgs_str", e.target.value as string)
                    }
                    onBlur={onBlur}
                    margin="dense"
                    InputLabelProps={{ shrink: true }}
                    helperText={
                        dcProject.coop_orgs_str
                            ? "When this field is empty the members below will be used"
                            : undefined
                    }
                />
            </Grid>
            <Grid item xs={12}>
                <Autocomplete<Item>
                    values={dcProject.cooperating_organisations}
                    label="NOT WORKING YET - Cooperating organisations (link to members)"
                    onChange={(orgs) =>
                        setField("cooperating_organisations", orgs)
                    }
                    dataSource={{
                        filter: { filter: { type: "affiliate" } },
                        xhrUrl: route("admin.items.index"),
                    }}
                    getOptionLabel={getContentTitleOptionLabel}
                />
            </Grid>
            <Grid item xs={12}>
                <TextField
                    value={dcProject.countries_str ?? ""}
                    label="Countries"
                    variant="outlined"
                    fullWidth
                    onChange={(e) =>
                        setField("countries_str", e.target.value as string)
                    }
                    onBlur={onBlur}
                    margin="dense"
                    InputLabelProps={{ shrink: true }}
                />
            </Grid>

            <Grid item xs={12}>
                <TextField
                    value={dcProject.topics_str ?? ""}
                    label="Host organisations"
                    variant="outlined"
                    fullWidth
                    onChange={(e) =>
                        setField("topics_str", e.target.value as string)
                    }
                    onBlur={onBlur}
                    margin="dense"
                    InputLabelProps={{ shrink: true }}
                />
            </Grid>
            <Grid item xs={4}>
                <DatePicker
                    date={dcProject.started_at}
                    label="Started at"
                    onChange={(date) => setField("started_at", date)}
                />
            </Grid>
            <Grid item xs={4}>
                <DatePicker
                    date={dcProject.ended_at}
                    label="Ended at"
                    onChange={(date) => setField("ended_at", date)}
                />
            </Grid>

            {DCPROJECT_FIELDS.map((field, idx) => (
                <Grid item xs={4} key={idx}>
                    <TextField
                        value={((dcProject as any)[field] as string) ?? ""}
                        label={pascalCase(field)}
                        variant="outlined"
                        fullWidth
                        onChange={(e) =>
                            setField(field, e.target.value as string)
                        }
                        onBlur={onBlur}
                        margin="dense"
                        InputLabelProps={{ shrink: true }}
                    />
                </Grid>
            ))}
            {DCPROJECT_HTMLFIELDS.map((field, idx) => (
                <Grid item xs={12} key={idx}>
                    <FormControl variant="outlined" fullWidth>
                        <InputLabel className={classes.labelOnOutline} shrink>
                            {pascalCase(field)}
                        </InputLabel>
                        <TextEditor
                            text={(dcProject as any)[field] || ""}
                            mode="limited"
                            // onChange={(text) => setField(field, text)}
                            onChangeJson={(json) => console.warn("todo", json)}
                        />
                    </FormControl>
                </Grid>
            ))}
            <Grid item xs={12}>
                <Typography variant="body1">
                    Note that the blurb and content fields below are not used
                    for Development Cooperation project items.
                </Typography>
            </Grid>
        </Grid>
    );
};

export default EditorDCProject;
