import React, { useState } from "react";

import {
    Box,
    Button,
    Grid,
    Paper,
    TablePagination,
    Typography,
} from "@mui/material";
import clsx from "clsx";

import { findContent } from "../../Models";
import Item from "../../Models/Item";
import ImageCard from "../General/ImageCard";
import { IDataSource } from "../useDataSource";

import useStyles from "./ImagePicker.styles";

interface IProps {
    dataSource: IDataSource<Item>;
    onSelect: (item: Item) => void;
    onCancel: () => void;
}
export const ImagePicker: React.FC<IProps> = ({
    dataSource,
    onCancel,
    onSelect,
}) => {
    const classes = useStyles();
    const { setPage, paginatedData } = dataSource;
    const [selectedItem, setSelectedItem] = useState<Item>();

    if (!paginatedData?.data?.length) {
        return <>- no images found -</>;
    }
    return (
        <>
            <Grid container spacing={1} className={classes.container}>
                {paginatedData.data.map((item, idx) => (
                    <Grid
                        key={idx}
                        item
                        xs={2}
                        onClick={() =>
                            setSelectedItem((selectedItem) =>
                                item === selectedItem ? undefined : item
                            )
                        }
                        className={clsx({
                            // [classes.selected]: item === selectedItem,
                        })}
                    >
                        <ImageCard image={item} imageOnly />
                    </Grid>
                ))}
            </Grid>
            {selectedItem && (
                <Paper
                    className={classes.preview}
                    elevation={5}
                    onClick={() => setSelectedItem(undefined)}
                >
                    <ImageCard image={selectedItem} imageOnly />
                    <Box>
                        <Typography variant="body1">
                            caption:{" "}
                            {findContent(selectedItem)?.subtitle ?? "-none-"}
                        </Typography>
                    </Box>
                    <Box>
                        <Typography variant="body1">
                            languages:{" "}
                            {selectedItem.contents
                                .map(({ lang }) => lang)
                                .join(", ")}
                        </Typography>
                    </Box>
                </Paper>
            )}
            <Box
                className={classes.footer}
                display="flex"
                justifyContent="space-between"
            >
                <TablePagination
                    component="div"
                    count={paginatedData.total}
                    rowsPerPage={paginatedData.per_page}
                    page={paginatedData.current_page - 1}
                    onPageChange={(evt, newPage) => setPage(newPage + 1)}
                    rowsPerPageOptions={[]}
                />
                <Box display="flex" alignItems="center">
                    <Button
                        color="primary"
                        variant="text"
                        onClick={() => onCancel && onCancel()}
                        className={classes.button}
                    >
                        Cancel
                    </Button>
                    <Button
                        color="primary"
                        disabled={!selectedItem}
                        onClick={() => onSelect(selectedItem!)}
                        variant="contained"
                        className={classes.button}
                    >
                        Select
                    </Button>
                </Box>
            </Box>
        </>
    );
};

export default ImagePicker;
