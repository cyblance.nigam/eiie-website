import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    container: {
        paddingBottom: 52,
    },
    footer: {
        position: "absolute",
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: theme.palette.background.default,
    },
    button: {
        marginRight: theme.spacing(2),
    },
    selected: {
        transform: "scale(1.4)",
    },
    preview: {
        position: "absolute",
        padding: theme.spacing(2),
        top: "10%",
        left: "50%",
        transform: "translateX(-50%)",
        width: "50%",
        maxWidth: 400,
        zIndex: theme.zIndex.appBar + 10,
    },
}));

export default useStyles;
