import { makeStyles } from "@mui/styles";
const useStyles = makeStyles(() => ({
    container: {
        minWidth: 500,
    },
}));

export default useStyles;
