import { makeStyles } from "@mui/styles";
const useStyles = makeStyles(() => ({
    imageCard: { width: "100%", maxWidth: 200 },
}));

export default useStyles;
