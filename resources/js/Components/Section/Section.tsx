import React, { ReactNode, useEffect, useState } from "react";

import { Box, Collapse, Paper, Typography } from "@mui/material";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import clsx from "clsx";

import useStyles from "./Section.styles";

interface IProps {
    title: string;
    // icon?: ReactNode;
    summary?: ReactNode;
    open?: boolean;
    onOpen?: (open: boolean) => void;
}
export const Section: React.FC<IProps> = ({
    title,
    // icon,
    summary,
    open,
    onOpen,
    children,
}) => {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(open);

    useEffect(() => {
        setIsOpen(open);
    }, [open]);

    useEffect(() => {
        onOpen && onOpen(!!isOpen);
    }, [isOpen, onOpen]);

    return (
        <Paper className={classes.section}>
            <Box
                display="flex"
                justifyContent="flex-start"
                alignItems="center"
                onClick={() => setIsOpen((isOpen) => !isOpen)}
                className={clsx(classes.header, {
                    [classes.headerOpen]: isOpen,
                })}
            >
                <Typography variant="h5">{title}</Typography>
                {isOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}
            </Box>
            <Collapse in={!isOpen}>
                <Box onClick={() => setIsOpen(true)}>{summary}</Box>
            </Collapse>
            <Collapse in={isOpen}>{children}</Collapse>
        </Paper>
    );
};

export default Section;
