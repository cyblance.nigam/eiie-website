<a href="{{ $href }}" {!! $class !!} rel="noopener">{{ $slot->isEmpty() ? $title : $slot }}</a>
