<div class="searchToggle"></div>
<form method="get" action="{{ route('search') }}" id="search_container{{ $suffix ?? '' }}">
    <input name="keyword" type="text" placeholder="{{ __('eiie.search') }}" id="search_input{{ $suffix ?? '' }}"/>
    <button aria-label="{{ __('eiie.search') }}" id="search_button{{ $suffix ?? '' }}" type="submit">{{ __('eiie.search') }}</button>
    <button aria-label="{{ __('cancel') }} " type="reset" id="search_cancel_button{{ $suffix ?? '' }}">{{ '⨯' }}</button>
</form>
