<section id="languageSelection{{ $suffix ?? '' }}">
<button id="languageSelection_toggle{{ $suffix ?? '' }}" aria-label="{{ __('eiie.Show languages') }}"></button>
<ul id="languageSelection_list{{ $suffix ?? '' }}">
@foreach (config('app.locales') as $localeKey => $locale)
    @if ($localeKey != app()->getLocale())
        @php 
            $segs = Request::segments();
            $segs[0] = $localeKey;
        @endphp 
        <li><a href="/{{ implode('/', $segs) }}">
            {{ $locale }}
        </a></li>
    @else 
        <li class="active"><span>{{ $locale }}</span></li>
    @endif 
@endforeach
</ul>
</section>
