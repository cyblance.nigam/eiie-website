@extends('collection.dossier')



@section('collection_header')
    @parent 

    @foreach ($collection->parentCollectionsOfType('dossier') as $dossier)
        <x-link :collection="$dossier" class="link_back" />
    @endforeach

@endsection

@section('collection_content')

<main id="collection_main" class="collection_sub collection_layout_resolutions">

    <ol class="collection_content">
			@php
			$items = isset($items) ? $items : $collection->items()->paginate(config('eiie.pagination_size', 18));
			@endphp
			@foreach ($items as $item) 
				<li>
					@include('shared.card')
				</li>
			@endforeach
		</ol>
		{{-- pagination --}}
		{{ $items->links() }}

</main>

@endsection 
