{{-- @extends('collection.dossier') --}}

@extends('collection.default')

@section('collection_header')
    @parent 

    @foreach ($collection->parentCollectionsOfType('dossier') as $dossier)
        <x-link :collection="$dossier" class="link_back" />
    @endforeach

@endsection

